<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Lists;
use App\Elements;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class ListController extends Controller
{

    public function index($element_id)
    {
        $list = Lists::where('element_id', $element_id)->orderBy('list_name', 'desc')->get();
        $elements = Elements::select('element_id', 'element_name')->get();
        $id = $element_id;
        return view('admin.admin_list', compact('list', 'elements', 'id'));

    }

    public function store(Request $request)
    {

        $this->validate($request, [
           'list_name' => 'required',
           'element_id' => 'required',
        ]);

                $list_name = !empty(Input::get('list_name')) ? Input::get('list_name') : '';

                $today = Carbon::now()->toDateTimeString();

                $list = new Lists;

                $list->list_name = $list_name;

                $list->element_id = Input::get('element_id');

                $list->created_at = $today;

                $list->updated_at = null;

                $list->save();

                Session::flash('message', 'The item have been saved.');
                return Redirect::to('admin_list/'.Input::get('element_id'));


    }

    public function search(Request $request, $element_id)
    {

          $term = Input::get('searchText');
  
          $results = array();
          
          $queries = Lists::where('list_name', 'LIKE', '%'.$term.'%')->where('element_id', $element_id)
            ->take(10)->get();
          
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->list_id, 'name' => $query->list_name ];
          }
        return response()->json($results);

    }

    public function edit($element_id, $list_id)
    {

        $list = Lists::where('list_id', $list_id)->where('element_id', $element_id)->first();

        return view('admin.edit.editList', compact('list'));



    }

    public function save($element_id, $list_id)
    {
                $id = $element_id;

                $list_name = !empty(Input::get('list_name')) ? Input::get('list_name') : '';

                $today = Carbon::now()->toDateTimeString();

                $list = Lists::find($list_id);

                $list->updated_at = $today;

                $list->save();


        Session::flash('message', 'The Job Title have been saved.');
        return Redirect::to('admin_list/'.$id);



    }

    public function destroy($element_id, $list_id)
    {
        $id = $element_id;

        $list = Lists::where('list_id', $list_id)->delete();

        return Redirect::to('admin_list/'.$id)->withErrors('Item was Successfully Deleted!');
    }


}
