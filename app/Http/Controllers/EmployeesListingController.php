<?php

namespace app\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use App\DataTables\EmployeesDataTable;
use DB;
use Session;
use App\DataTables\Scopes\SearchScope;
use App\DataTables\Scopes\ACLScope;

class EmployeesListingController extends Controller
{
    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $htmlBuilder;

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function getBasic(Request $request, EmployeesDataTable $dataTable)
    {
        if ($request->ajax()) {
            return $dataTable->addScope(new SearchScope)->ajax();
        }

        $html = $dataTable->html();

        return view('search.all', compact('html'));
    }

}
