<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\ClientIntegrationDash;
use App\ClientIntegration;
use App\Source;
use App\Client;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminUnmappedController extends Controller
{

    public function index()
    {
        $integrations = ClientIntegrationDash::select()->get();
        $sources_dd = Source::orderBy('source', 'asc')->get();
        $clients_dd = Client::orderBy('client', 'asc')->get();
        return view('admin.admin_unmapped', compact('integrations', 'sources_dd', 'clients_dd'));

    }

    public function store(Request $request)
    {

        $this->validate($request, [
           'new_client_id' => 'required',
           'new_src_type_id' => 'required',
        ]);     

        if((!empty(Input::get('new_client_id'))) || (!empty(Input::get('new_src_type_id')))) {

          foreach (Input::get('new_client_id') AS $key => $value){

            foreach (Input::get('new_src_type_id') AS $key2 => $value2){

                $check =  ClientIntegration::where('client_id', $value)->where('src_type_id', $value2)->first();                         
                  if (empty($check)){

                    $today = Carbon::now()->toDateTimeString();

                    $client_integration = new ClientIntegration;

                    $client_integration->client_id = $value;

                    $client_integration->src_type_id = $value2;

                    $client_integration->ignore = $request->ignore;

                    $client_integration->created_at = $today;

                    $client_integration->updated_at = null;

                    $client_integration->save();

                  }

              }
            }

          }
                Session::flash('message', 'Successfully mapped Source Systems to Clients');
                return Redirect::to('admin_unmapped');


    }

    public function edit($client_id, $src_type_id)
    {

        $integ_client_map = ClientIntegrationDash::where('client_id', $client_id)->where('src_type_id', $src_type_id)->first();

        return view('admin.edit.editIDMMap', compact('integ_client_map'));

    }

    public function save(Request $request, $client_id, $src_type_id)
    {


                $today = Carbon::now()->toDateTimeString();

                $client_integration = ClientIntegration::select()
                ->where('client_id', '=', $client_id)
                ->where('src_type_id', '=', $src_type_id)
                ->update(['ignore' => $request->ignore, 'updated_at' => $today]);


        Session::flash('message', 'Changes have been saved.');
        return Redirect::to('admin_unmapped');

    }

    public function searchsource(Request $request)
    {

          $term = Input::get('searchText');
  
          $results = array();
          $queries = Source::where('source', 'LIKE', '%'.$term.'%')->orderBy('source', 'asc')
            ->take(100)->get();
          
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->src_type_id, 'name' => $query->source ];
          }
        return response()->json($results);

    }

    public function searchclient(Request $request)
    {

          $term = Input::get('searchText');
  
          $results = array();
          $queries = Client::where('client', 'LIKE', '%'.$term.'%')->orderBy('client', 'asc')
            ->take(100)->get();
          
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->client_id, 'name' => $query->client ];
          }
        return response()->json($results);

    }


}
