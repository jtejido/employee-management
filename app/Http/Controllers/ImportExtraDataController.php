<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\IDM;
use App\Elements;
use App\Lists;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use View, Session, Exception, Excel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Services\ExtraElementData;
use App\Services\ImportTemplate;

class ImportExtraDataController extends Controller
{
  public function __construct(ImportTemplate $import) {
    $this->import = $import;
  }

  public function index()
  {
    return view('import.extra_data');

  }
  
  public function downloadtemplate() {
        $row = null;
        $file = $this->import->TemplateExtraData($row)->download("xlsx");

    }


    public function uploadtemplate() {
              
                    Excel::selectSheets('EmployeeTemplate')->load(Input::file('upload'), function ($reader) {
                        Session::forget('import_extra_data.row');
                        $results = $reader->get()->toArray();
                        $row = array();

                                //var_dump($results);exit;

                                foreach ($results as $key => $value) {

                                        if (!empty($value['employee_id'])){

                                            $start_dte = Carbon::parse($value['start_dte'])->format('m/d/Y');

                                            if ((empty($value['start_dte'])) || (empty($value['element_name'])) || (empty($value['value']))) {
                                                $error = "Some columns are not filled in.";
                                                array_push($row, array('id' => $value['employee_id'], 'id_type' => $value['employee_id_type'], 'element_name' => $value['element_name'], 'element_value' => $value['value'], 'start_dte' => $start_dte, 'error' => $error));
                                            }

                                            else {
                                                
                                                $temp_id = $value['employee_id'];

                                                if ($value['employee_id_type'] === "WIN ID"){
                                                $id = Employee::select('empl_id')->where('username', $temp_id)->first();
                                                    if (!empty($id)){
                                                        $empl_id = $id->empl_id;
                                                    }
                                                    else {
                                                        $empl_id = null;
                                                        $error = "ID not found";
                                                    }
                                                }
                                                else if ($value['employee_id_type'] === "Synergy"){
                                                $id = IDM::select('empl_id')->where('src_type_id', 28)->where('src_id', $temp_id)->whereNotNull('end_dte')->first();
                                                    if (!empty($id)){
                                                        $empl_id = $id->empl_id;
                                                    }
                                                    else {
                                                        $empl_id = null;
                                                        $error = "ID not found";
                                                    }
                                                }
                                                else if ($value['employee_id_type'] === "Employee ID"){
                                                $id = Employee::select('empl_id')->where('empl_id', $temp_id)->first();
                                                    if (!empty($id)){
                                                        $empl_id = $id->empl_id;
                                                    }
                                                    else {
                                                        $empl_id = null;
                                                        $error = "ID not found";
                                                    }
                                                }

                                                $element_id = Elements::select('element_id')->where('element_name', $value['element_name'])->first();

                                                if ($element_id->element_id === 2){
                                                    $value['value'] = Carbon::parse($value['value'])->format('m/d/Y');
                                                }

                                                if ($element_id->element_id === 1){
                                                    $check = Lists::where('list_name', $value['value'])->first();
                                                     if(empty($check)){
                                                        $empl_id = null;
                                                        $error = "The Job Title you attempted to map is not found on the list. Please make sure the Job Title is selected from the list.";
                                                     }
                                                }

                                                if (!empty($empl_id)){
                                                    $id = new ExtraElementData;

                                                    $result = $id->addValue($empl_id, $value['value'], $element_id->element_id, $start_dte);

                                                        if($result !== true) {
                                                          array_push($row, array('id' => $value['employee_id'], 'id_type' => $value['employee_id_type'], 'element_name' => $value['element_name'], 'element_value' => $value['value'], 'start_dte' => $start_dte, 'error' => $result));

                                                        }
                                                }
                                                else {
                                                    array_push($row, array('id' => $value['employee_id'], 'id_type' => $value['employee_id_type'], 'element_name' => $value['element_name'], 'element_value' => $value['value'], 'start_dte' => $start_dte, 'error' => $error));
                                                }
                                            }
                                            
                                        }
                                }
                          Session::put('import_extra_data.row', $row);
                  });

                  Session::flash('message', 'Users uploaded successfully. If there are errors, please see them below and re-download the sheet using the "Download Table" button below, and re-upload as necessary.');
                  return Redirect::to('import_extra_data');
              
    }


    public function errortemplate() {
        $row = Session::get('import_extra_data.row');
        $file = $this->import->TemplateExtraData($row)->download("xlsx");

    }

}
