<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\IDM;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\IDManagement;

class IDMController extends Controller
{

    public function addRedirect(Request $request)
    {

        $this->validate($request, [
           'start_dte' => 'required|date',
           'src_type_id' => 'required|integer',
           'src_id' => 'required',
           'empl_id' => 'required|integer',
        ]);

        $id = new IDManagement;
        $result = $id->addID($request->empl_id, $request->src_id, $request->src_type_id, $request->start_dte);

        if($result === true) {

          return back()->withInput();

        } else {

          return back()->with('status', $result);

        }

    }

}
