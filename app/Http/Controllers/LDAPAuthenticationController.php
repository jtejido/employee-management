<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Conduent\LDAP;
use Auth;
use App\Employee;
use Illuminate\Http\Request;
use View;
use Session;
use Redirect;
use App\ACL;

class LDAPAuthenticationController extends Controller {

  public function __construct(LDAP $ldap) {
    $this->ldap = $ldap;
  }

  public function login(Request $request) {

      Auth::logout();

      Session::flush();

      if(!env('APP_DEBUG', true)) {
        if (!$this->ldap->authenticate($request->username, $request->password, env('LDAP_DOMAIN'), env('LDAP_SERVER'))) {
          return back()->with('error', 'Username and/or password are incorrect.');
        }
      }

      $user = Employee::where('username', strtolower($request->username))->first();

      if(!isset($user)) {
        return back()->with('error', 'No profile configured for this user. Please contact support.');
      }

      if($user->super) {
        Session::put('user.super', true);
      } else {

        $sites = ACL::select('site_id')->groupBy('site_id')->where('empl_id', $user->empl_id)->get();

        $clients = ACL::select('client_id')->groupBy('client_id')->where('empl_id', $user->empl_id)->get();

        if((!$sites->contains('site_id', '0')) && (!$clients->contains('client_id', '0'))) {
          Session::put('user.super', true);
        }

        if(!$sites->contains('site_id', '0')) {
          Session::put('user.site', collect($sites->toArray())->flatten()->toArray());
        }

        if(!$clients->contains('client_id', '0')) {
          Session::put('user.client', collect($clients->toArray())->flatten()->toArray());
        }
      }

      Auth::login( $user );

      return Redirect::to('/');

  }


  public function logout() {
    Auth::logout();
    Session::flush();
    return Redirect::to('login')->with('message', 'You just logged out.');
  }

  public function samllogout() {
    return \Saml2::logout();
  }

  public function index(Request $request) {

    return View::make('login');
  }

  public function samlindex(Request $request) {

    return \Saml2::login(\URL::full());
  }


  public function indexImposter(Request $request) {
    return View::make('imposter');
  }

}
