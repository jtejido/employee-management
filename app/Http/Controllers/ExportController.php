<?php

namespace App\Http\Controllers;
ini_set('memory_limit', '-1');
use App\Http\Controllers\Controller;
use App\Employee;
use App\IDM;
use App\Source;
use App\GoogleID;
use App\UnmappedIDExport;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Excel;
use Session;

class ExportController extends Controller
{
  public function index()
  {
        $clients = Employee::ApplyACL()->select(['client','client_id']);
          if(Session::has('user.client')) {
            $clients = $clients->whereIn('client_id', Session::get('user.client'));
          }
            $clients = $clients->groupBy(['client','client_id'])->orderBy('client')->get();

        return view('exports', compact('clients'));

  }
  public function exportEmployeeList()
  {
    Excel::create('EmployeeRoster', function($excel) {

        $excel->sheet('EmployeeRoster', function($sheet) {
          $query = Employee::ApplyACL()->select(['name','client','site','manager'])->get();

          $sheet->fromModel($query);

          $sheet->row(1, function($row) {

              $row->setBackground('#fd8924');
              $row->setFontColor('#ffffff');

          });

        });

    })->export('xlsx');
  }

  public function exportNewHireList()
  {
    Excel::create('NewHireEmployeeRoster', function($excel) {

        $excel->sheet('NewHireEmployeeRoster', function($sheet) {
          $query = Employee::ApplyACL()->select(['name','client','site','manager','hire_dte'])->where('hire_dte', '>', Carbon::now()->subDay(30))->get();
          $sheet->fromModel($query);

          $sheet->row(1, function($row) {

              $row->setBackground('#fd8924');
              $row->setFontColor('#ffffff');

          });

        });

    })->export('xlsx');
  }

  public function exportAssignedID()
  {

    Excel::create('AssignedIDRoster', function($excel) {

        $excel->sheet('AssignedIDRoster', function($sheet) {
          $query = IDM::select(['employees.name', 'employees.manager', 'employees.client', 'employees.site', 'src_id', 'src_type_id', 'start_dte', 'end_dte'])->leftjoin('employees', 'idm.empl_id', '=', 'employees.empl_id');

          if(Session::has('user.client')) {
            $query->whereIn('employees.client_id', Session::get('user.client'));
          }

          if(Session::has('user.site')) {
            $query->whereIn('employees.site_id', Session::get('user.site'));
          }

          $query = $query->get();

          $sheet->fromModel($query);

          $sheet->row(1, function($row) {

              $row->setBackground('#fd8924');
              $row->setFontColor('#ffffff');

          });

        });

    })->export('xlsx');
  }

  public function exportGoogleID()
  {

    Excel::create('EmployeeGoogleIDRoster', function($excel) {

        $excel->sheet('EmployeeGoogleIDRoster', function($sheet) {
          $query = GoogleID::select(['name', 'ldap', 'oneacd', 'impact360', 'synergy_id', 'win_id']);

          if(Session::has('user.client')) {
            $query->whereIn('client_id', Session::get('user.client'));
          }

          if(Session::has('user.site')) {
            $query->whereIn('site_id', Session::get('user.site'));
          }

          $query = $query->get();

          $sheet->fromModel($query);

          $sheet->row(1, function($row) {

              $row->setBackground('#fd8924');
              $row->setFontColor('#ffffff');

          });

        });

    })->export('xlsx');
  }

  public function exportMissingIDM($client_id)
  {

    Excel::create('EmployeesWithMissingIDs', function($excel) use ($client_id) {

        $excel->sheet('EmployeesWithMissingIDs', function($sheet) use ($client_id) {
          $query = UnmappedIDExport::select(['win_id AS Win ID', 'name AS Employee Name', 'site AS Site', 'client AS Client', 'source AS Missing Login Type'])->where('client_id', $client_id)->orderBy('name', 'asc')->orderBy('source', 'asc');

          if(Session::has('user.client')) {
            $query->whereIn('client_id', Session::get('user.client'));
          }

          if(Session::has('user.site')) {
            $query->whereIn('site_id', Session::get('user.site'));
          }

          $query = $query->get();

          $sheet->fromModel($query);

          $sheet->row(1, function($row) {

              $row->setBackground('#fd8924');
              $row->setFontColor('#ffffff');

          });

        });

    })->export('xlsx');
  }


}
