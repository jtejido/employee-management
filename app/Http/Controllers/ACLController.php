<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\ACL;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class ACLController extends Controller
{

    public function add(Request $request)
    {

      if(Session::get('user.super') !== true) {
        return back();
      }

      $acl = new ACL;

      $acl->empl_id = $request->empl_id;

      $acl->module_id = 0;

      $acl->client_id = $request->client_id;

      $acl->site_id = $request->site_id;

      $acl->edit = ($request->edit) ? true : false;

      $acl->save();

      return back();

    }

    public function delete($acl_id)
    {

      if(Session::get('user.super') !== true) {
        return back();
      }

      $acl = ACL::findOrFail($acl_id);

      $acl->delete();

      return back();

    }

}
