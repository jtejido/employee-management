<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\ManagerHistory;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\Manager;
use Illuminate\Support\Facades\Input;

class ManagerController extends Controller
{

    public function addRedirect(Request $request)
    {

        $this->validate($request, [
           'start_dte' => 'required|date',
           'new_manager_id' => 'required|integer',
           'manager_id' => 'required|integer',
           'empl_id' => 'required|integer',
        ]);

        $id = new Manager;

        $result = $id->addManager($request->empl_id, $request->start_dte, $request->new_manager_id, $request->manager_id);

        if($result === true) {

          return back()->withInput();

        } else {

          return back()->with('status', $result);

        }

    }

    public function search(Request $request)
    {

          $term = Input::get('searchText');
  
          $results = array();
          
          $queries = Employee::where('name', 'LIKE', '%'.$term.'%')
            ->take(10)->get();
          
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->empl_id, 'name' => $query->name ];
          }
        return response()->json($results);

    }

}
