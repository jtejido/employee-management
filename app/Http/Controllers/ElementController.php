<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\IDM;
use App\Lists;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\ExtraElementData;

class ElementController extends Controller
{

    public function addRedirect(Request $request)
    {

        $this->validate($request, [
           'start_dte' => 'required|date',
           'value' => 'required',
           'element_id' => 'required|integer',
           'empl_id' => 'required|integer',
        ]);

        $id = new ExtraElementData;
        $result = $id->addValue($request->empl_id, $request->value, $request->element_id, $request->start_dte);

        if($result === true) {

          return back()->withInput();

        } else {

          return back()->with('status', $result);

        }

    }

}
