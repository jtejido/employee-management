<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\UnmappedIDMMapAll;
use App\UnmappedIDM;
use App\ClientIntegration;
use App\Employee;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Services\IDManagement;
use Excel;
use App\DataTables\UnmappedIDMTable;
use Yajra\Datatables\Html\Builder;

class UnmappedIDMController extends Controller
{

    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $htmlBuilder;

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function index()
    {
        $integrations = UnmappedIDMMapAll::distinct()->select('source','src_type_id');
          if(Session::has('user.client')) {
            $integrations = $integrations->whereIn('client_id', Session::get('user.client'));
          }
            $integrations = $integrations->get();

        return view('unmapped_idm', compact('integrations'));

    }

    public function view(Request $request, UnmappedIDMTable $dataTable, $src_type_id)
    {
        $request->session()->forget('unmapped_idm.integration');
        
        $request->session()->push('unmapped_idm.integration', $src_type_id);

        $integrations = UnmappedIDMMapAll::distinct()->select('source','src_type_id');
          if(Session::has('user.client')) {
            $integrations = $integrations->whereIn('client_id', Session::get('user.client'));
          }
            $integrations = $integrations->get();

        $check = UnmappedIDMMapAll::ApplyACL()->where('src_type_id', Session::get('unmapped_idm.integration'))->first();

        if (!empty($check)){
          if ($request->ajax()) {
              return $dataTable->ajax();
          }

          $html = $dataTable->html();
          return view('unmapped_idm.view', compact('html', 'integrations', 'src_type_id'));
        }
        else {
          Session::flash('message', 'No IDs available to map!');
          return view('unmapped_idm', compact('integrations'));
        }
       

    }

    public function edit($unmapped_id, $src_type_id)
    {

        $unmapped_id = UnmappedIDMMapAll::ApplyACL()->where('id', $unmapped_id)->first();

        return view('unmapped_idm.edit', compact('unmapped_id', 'src_type_id'));

    }

    public function save(Request $request)
    {

        $this->validate($request, [
           'start_dte' => 'required|date',
           'employee_name' => 'required',
        ]);

        $id = new IDManagement;
        $result = $id->addID($request->employee_id, $request->src_id, $request->src_type_id, $request->start_dte);

        if($result === true) {

          Session::flash('message', 'Successfully Mapped ID.');
            return Redirect::to('unmapped_idm/'.$request->src_type_id);

        } else {

        return back()->with('status', $result);

        }

    }

    public function ignore($unmapped_id, $src_type_id)
    {

        $unmapped_id = UnmappedIDM::ApplyACL()->find($unmapped_id);
        $unmapped_id->ignore = 1;
        $unmapped_id->save();

        Session::flash('message', $unmapped_id->src_id.' has been removed.');
        return Redirect::to('unmapped_idm/'.$src_type_id);

    }

    public function bulkignore(Request $request)
    {

        if (!empty($request->ids_to_ignore)) {
          $ids_to_ignore = $request->ids_to_ignore;
            
            foreach ($ids_to_ignore as $id_to_ignore) {
                $unmapped_id = UnmappedIDM::ApplyACL()->find($id_to_ignore);
                $unmapped_id->ignore = 1;
                $unmapped_id->save();
            }
            
            $message = 'These IDs have been removed.';
            return back()->with('status', $message);
        } else {
            $message = 'No Source IDs selected.';
            return back()->with('status', $message);
        }


    }

  public function export($src_type_id)
  {
    Excel::create('UnmappedIDs', function($excel) use ($src_type_id) {

        $excel->sheet('UnmappedIDs', function($sheet) use ($src_type_id) {
          $unmapped_ids = UnmappedIDMMapAll::ApplyACL()->select('client', 'source', 'src_id');

          if($src_type_id != -1){
            $unmapped_ids = $unmapped_ids->where('src_type_id', $src_type_id);
          }
            $unmapped_ids = $unmapped_ids->get();

          $sheet->fromModel($unmapped_ids);

          $sheet->row(1, function($row) {

              $row->setBackground('#fd8924');
              $row->setFontColor('#ffffff');

          });

        });

    })->export('xlsx');
  }


}
