<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\IDM;
use App\Source;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use View, Session, Exception, Excel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Services\IDManagement;
use App\Services\ImportTemplate;

class ImportIDMController extends Controller
{
  public function __construct(ImportTemplate $import) {
    $this->import = $import;
  }

  public function index()
  {
    $clients = Employee::ApplyACL()->select(['client','client_id'])->groupBy(['client','client_id'])->orderBy('client')->get();

    $sites = Employee::ApplyACL()->select(['site','site_id'])->groupBy(['site','site_id'])->orderBy('site')->get();

    return view('import.idm', compact('clients', 'sites'));

  }
  
  public function downloadtemplate() {
        $row = null;
        $file = $this->import->TemplateIDM($row)->download("xlsx");

    }


    public function uploadtemplate() {
              
                    Excel::selectSheets('EmployeeTemplate')->load(Input::file('upload'), function ($reader) {
                        Session::forget('import_idm.row');
                        $results = $reader->get()->toArray();
                        $row = array();

                                //var_dump($results);exit;

                                foreach ($results as $key => $value) {

                                        if (!empty($value['employee_id'])){

                                            $start_dte = Carbon::parse($value['start_dte'])->format('m/d/Y');

                                            if ((empty($value['start_dte'])) || (empty($value['id_source_system'])) || (empty($value['id_value']))) {
                                                $error = "Some columns are not filled in.";
                                                array_push($row, array('id' => $value['employee_id'], 'id_type' => $value['employee_id_type'], 'id_source_system' => $value['id_source_system'], 'id_value' => $value['id_value'], 'start_dte' => $start_dte, 'error' => $error));
                                            }

                                            else {
                                                
                                                $temp_id = $value['employee_id'];

                                                if ($value['employee_id_type'] === "WIN ID"){
                                                $id = Employee::select('empl_id')->where('username', $temp_id)->first();
                                                    if (!empty($id)){
                                                        $empl_id = $id->empl_id;
                                                    }
                                                    else {
                                                        $empl_id = null;
                                                        $error = "ID not found";
                                                    }
                                                }
                                                else if ($value['employee_id_type'] === "Synergy"){
                                                $id = IDM::select('empl_id')->where('src_type_id', 28)->where('src_id', $temp_id)->whereNotNull('end_dte')->first();
                                                    if (!empty($id)){
                                                        $empl_id = $id->empl_id;
                                                    }
                                                    else {
                                                        $empl_id = null;
                                                        $error = "ID not found";
                                                    }
                                                }
                                                else if ($value['employee_id_type'] === "Employee ID"){
                                                $id = Employee::select('empl_id')->where('empl_id', $temp_id)->first();
                                                    if (!empty($id)){
                                                        $empl_id = $id->empl_id;
                                                    }
                                                    else {
                                                        $empl_id = null;
                                                        $error = "ID not found";
                                                    }
                                                }

                                                $src_type_id = Source::select('src_type_id')->where('source', $value['id_source_system'])->first();

                                                if (!empty($empl_id)){
                                                    $id = new IDManagement;

                                                    $result = $id->addID($empl_id, $value['id_value'], $src_type_id->src_type_id, $start_dte);

                                                        if($result !== true) {
                                                          array_push($row, array('id' => $value['employee_id'], 'id_type' => $value['employee_id_type'], 'id_source_system' => $value['id_source_system'], 'id_value' => $value['id_value'], 'start_dte' => $start_dte, 'error' => $result));

                                                        }
                                                }
                                                else {
                                                    array_push($row, array('id' => $value['employee_id'], 'id_type' => $value['employee_id_type'], 'id_source_system' => $value['id_source_system'], 'id_value' => $value['id_value'], 'start_dte' => $start_dte, 'error' => $error));
                                                }
                                            }
                                            
                                        }
                                }
                          Session::put('import_idm.row', $row);
                  });

                  Session::flash('message', 'Users uploaded successfully. If there are errors, please see them below and re-download the sheet using the "Download Table" button below, and re-upload as necessary.');
                  return Redirect::to('import_idm');
              
    }


    public function errortemplate() {
        $row = Session::get('import_idm.row');
        $file = $this->import->TemplateIDM($row)->download("xlsx");

    }

    public function downloadtemplateclient($client_id) {
        $row = Employee::ApplyACL()->select(['username'])->where('client_id', $client_id)->get();
        $file = $this->import->TemplateIDM($row)->download("xlsx");

    }

    public function downloadtemplatesite($site_id) {
        $row = Employee::ApplyACL()->select(['username'])->where('site_id', $site_id)->get();
        $file = $this->import->TemplateIDM($row)->download("xlsx");

    }


}
