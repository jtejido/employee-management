<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\EmployeeIntegrationIDMMap;
use App\Source;
use App\Client;
use App\Lists;
use App\Elements;
use App\Site;
use App\IDM;
use App\Module;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class EmployeeController extends Controller
{

    public function profile(Request $request, $empl_id)
    {

        $employee = Employee::ApplyACL()->where('empl_id', $empl_id)
          ->firstorfail()
          ->load('idm', 'idm.source', 'acl', 'acl.client', 'acl.site', 'idm.source', 'managerHistory', 'managerHistory.manager_employee', 'extradata', 'extradata.elements');
        $sources = Source::orderBy('source', 'asc')->get();
        $elements = Elements::orderBy('element_name', 'desc')->get();
        $lists = Elements::select('element_id')->where('is_list', 1)->get();
              foreach ($lists as $r){
                $list_temp[] = $r->element_id;
              }
        $list = json_encode($list_temp);
        $clients = Client::orderBy('client', 'asc')->get();
        $sites = Site::orderBy('name', 'asc')->get();

        $id_assigned = EmployeeIntegrationIDMMap::where('empl_id', $empl_id)->orderBy('source')->get();
 
        return view('employee.profile', compact('employee', 'sources', 'sites', 'clients', 'elements', 'list', 'id_assigned'));

    }

    public function team($empl_id)
    {

        $employees = Employee::ApplyACL()->where('manager_id', $empl_id)->get();

        return view('employee.team', compact('employees'));

    }


}
