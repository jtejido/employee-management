<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class AdminController extends Controller
{

    public function index()
    {
        $admins = Employee::ApplyACL()->where('super', true)->get();
        return view('admin.admin_users', compact('admins'));

    }

    public function store(Request $request)
    {

        $this->validate($request, [
           'new_admin' => 'required',
        ]);

                $employee = Employee::find($request->new_admin_id);

                $employee->super = true;

                $employee->save();

                Session::flash('message', 'The User have been added.');
                return Redirect::to('admin_users');


    }

    public function search(Request $request)
    {

          $term = Input::get('searchText');
  
          $results = array();
          
          $queries = Employee::where('name', 'LIKE', '%'.$term.'%')
            ->take(10)->get();
          
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->empl_id, 'name' => $query->name ];
          }
        return response()->json($results);

    }

    public function remove($empl_id)
    {

                $employee = Employee::find($empl_id);

                $employee->super = false;

                $employee->save();


        Session::flash('message', 'The User have been removed.');
        return Redirect::to('admin_users');



    }

}
