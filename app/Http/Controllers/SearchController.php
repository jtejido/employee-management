<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\Source;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class SearchController extends Controller
{

    public function index(Request $request)
    {

        $clients = Employee::ApplyACL()->select(['client','client_id'])->groupBy(['client','client_id'])->orderBy('client')->get();

        $sites = Employee::ApplyACL()->select(['site','site_id'])->groupBy(['site','site_id'])->orderBy('site')->get();

        return view('search', compact('clients', 'sites'));

    }

    public function search(Request $request)
    {

        $request->session()->forget('search');

        if(isset($request->client)) {
          $request->session()->forget('search.client');
          foreach($request->client as $client) {
            $request->session()->push('search.client', $client);
          }
        }

        if(isset($request->site)) {
          $request->session()->forget('search.site');
          foreach($request->site as $site) {
            $request->session()->push('search.site', $site);
          }
        }

        if(isset($request->name)) {
          $request->session()->forget('search.name');
          $request->session()->push('search.name', '%'.$request->name.'%');
        }

        if(isset($request->status)) {
        $request->session()->forget('search.status');
        $request->session()->push('search.filter', $request->search);
        }

        if(isset($request->filter)) {
        $request->session()->forget('search.filter');
        $request->session()->push('search.filter', $request->filter);
        }

        return redirect('employees');
    }

    public function searchName(Request $request)
    {

        $request->session()->forget('search');

        if(isset($request->name)) {
          $request->session()->forget('search.name');
          $request->session()->forget('search.status');
          $request->session()->forget('search.filter');
          $request->session()->push('search.status', $request->status);
          $request->session()->push('search.name', '%'.$request->name.'%');
          $request->session()->push('search.filter', $request->filter);
        }

        return redirect('employees');
    }

    public function forget(Request $request)
    {

        $request->session()->forget('search');
        return redirect('employees');
    }

    public function searchNewHire(Request $request)
    {

        $request->session()->forget('search');
        $request->session()->forget('search.newhire');
        $request->session()->push('search.newhire', true);

        return redirect('employees');
    }


}
