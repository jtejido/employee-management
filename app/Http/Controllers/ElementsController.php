<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Elements;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;

class ElementsController extends Controller
{

    public function index()
    {
        $elements = Elements::orderBy('element_name', 'desc')->get();
        return view('admin.admin_elements', compact('elements'));

    }

    public function store(Request $request)
    {

        $this->validate($request, [
           'element_name' => 'required',
        ]);

                $element_name = !empty(Input::get('element_name')) ? Input::get('element_name') : '';

                $today = Carbon::now()->toDateTimeString();

                $elements = new Elements;

                $elements->element_name = $element_name;

                $elements->created_at = $today;

                $elements->updated_at = null;

                $elements->save();

                Session::flash('message', 'The Element have been saved.');
                return Redirect::to('admin_elements');


    }

    public function edit($element_id)
    {

        $element = Elements::where('element_id', $element_id)->first();

        return view('admin.edit.editElement', compact('element'));



    }

    public function save($element_id)
    {

                $element_name = !empty(Input::get('element_name')) ? Input::get('element_name') : '';

                $today = Carbon::now()->toDateTimeString();

                $elements = Elements::find($element_id);

                $elements->element_name = $element_name;

                $elements->is_list = Input::get('is_list');

                $elements->updated_at = $today;

                $elements->save();


        Session::flash('message', 'The Element have been saved.');
        return Redirect::to('admin_elements');



    }

    public function destroy($element_id)
    {
        $id = $element_id;

        $list = Elements::where('element_id', $element_id)->delete();

        return Redirect::to('admin_elements')->withErrors('Item was Successfully Deleted!');
    }


}
