<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Employee;
use App\UnmappedIDM;
use App\IDM;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use App\ACL;
use Auth;

class OverviewController extends Controller
{

    public function overview(Request $request)
    {

      $employees = Employee::ApplyACL()->where('term_dte', '0000-00-00')->count();
      $new_hires = Employee::ApplyACL()->where('hire_dte', '>', Carbon::now()->subDay(30))->count();
      $unmapped_ids = UnmappedIDM::ApplyACL()->count();
      $terminated = Employee::ApplyACLTermed()->count();
      $acls = ACL::where('empl_id', Auth::User()->empl_id)->get()->load('client', 'site');

        return view('overview', compact('employees', 'new_hires', 'unmapped_ids', 'terminated', 'acls'));

    }


}
