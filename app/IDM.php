<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IDM extends Model
{

  protected $table = 'idm';

  public function source()
  {
    return $this->hasOne('App\Source', 'src_type_id', 'src_type_id');
  }
  public function employee()
  {
      return $this->belongsTo('App\Employee', 'empl_id', 'empl_id');
  }

}
