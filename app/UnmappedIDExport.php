<?php

namespace App;
use Session;
use Illuminate\Database\Eloquent\Model;

class UnmappedIDExport extends Model
{

	protected $table = 'employee_integration_unmappedidm_map';

}
