<?php

namespace App;
use Session;
use Illuminate\Database\Eloquent\Model;

class UnmappedIDMMapAll extends Model
{

  protected $table = 'unmapped_idm_integration_map';


    public function scopeApplyACL($query)
  {

      if(Session::get('user.super') == true) {
        return $query;
      }

      if(Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('user.client'));
      }

      return $query;

  }

}
