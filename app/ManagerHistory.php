<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerHistory extends Model
{
  protected $table = 'manager_histories';

  public function manager_employee()
  {
    return $this->hasOne('App\Employee', 'empl_id', 'manager_id');
  }
  public function employee()
  {
      return $this->belongsTo('App\Employee', 'empl_id', 'empl_id');
  }

}
