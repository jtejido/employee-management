<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeIDMap extends Model
{
   protected $table = 'employee_idm_map';

  public function employee()
  {
      return $this->belongsTo('App\Employee', 'empl_id', 'empl_id');
  }
}
