<?php

namespace App;
use Session;
use Illuminate\Database\Eloquent\Model;

class UnmappedIDM extends Model
{

  protected $table = 'unmappedidm';

  public function source()
  {
    return $this->hasOne('App\Source', 'src_type_id', 'src_type_id');
  }


  public function scopeApplyACL($query)
  {

      if(Session::get('user.super') == true) {
        return $query;
      }

      if(Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('user.client'));
      }

      return $query;

  }

}
