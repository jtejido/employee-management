<?php

namespace App\Services;
use App\Employee;
use App\ExtraData;
use Carbon\Carbon;

class ExtraElementData
{


  /**
   * Employee empl_id
   * @var Integer
   */
  private $empl_id;

  /**
   * Employees new element value
   * @var string
   */
  private $value;

  /**
   * Employees new element_id
   * @var Integer
   */
  private $element_id;

  /**
   * Start Date of new Manager
   * @var object Carbon\Carbon
   */
  private $start_dte;


  public function addValue($empl_id, $value, $element_id, $start_dte) {

    $this->empl_id = $empl_id; 

    $this->element_id = $element_id;

    $this->value = $value;

    $this->start_dte = Carbon::parse($start_dte);

    $inactivedata = $this->getInactiveData();

    if($inactivedata !== false) {

      $date = $this->getDate();

      if($this->start_dte->between($date['sdate'], $date['edate'])) {
          return "The start_dte of the element you've attempted to map is between a start and end date of a previously assigned element. Please make sure that the date doesn't overlap with previously assigned element.";
      }

    }

    $activeData = $this->getActiveData();

    if($activeData !== false) {

      if($this->maxStartDate() >= $this->start_dte) {
          return "The Element you've attempted to map has already been mapped with a value in the past before the effective date. The Element was not mapped.";
        }

        $this->expireOldData();
    }

    $this->createNewData();

    return true;

  }

  private function createNewData() {
    $today = Carbon::now();

    $id = new ExtraData;

    $id->empl_id = $this->empl_id;

    $id->element_id = $this->element_id;

    if ($this->element_id === 2){
    $id->value = Carbon::parse($this->value)->format('m/d/Y');
    }
    else {
    $id->value = $this->value;
    }

    $id->start_dte = $this->start_dte->toDateString();

    $id->created_at = $today->toDateTimeString();

    $id->save();

  }

  private function expireOldData() {
    $today = Carbon::now();

    $empl_id = $this->empl_id;

    $element_id = $this->element_id;

    $end_dte =  $this->start_dte->toDateString();

    $updated_at = $today->toDateTimeString();

    $element_update = ExtraData::where('empl_id', $empl_id)->where('element_id', $element_id)->whereNull('end_dte')->first();

    if (!empty($element_update))
      {
        $element_update = ExtraData::select()
        ->where('empl_id', $empl_id)
        ->where('element_id', $element_id)
        ->whereNull('end_dte')
        ->update(['end_dte' => $end_dte, 'updated_at' => $updated_at]);
      }


  }

    private function getInactiveData() {

    $id = ExtraData::where('empl_id', $this->empl_id)->where('element_id', $this->element_id)->whereNotNull('end_dte')->orderBy('start_dte', 'desc')->first();

    if($id === null) {

      return false;

    } else {

      return $this->id = $id->id;

    }

  }

  private function getDate() {
    $currentid = $this->id;
    $date = ExtraData::where('id', $currentid)->first();
    $sdate = Carbon::parse($date->start_dte);
    $edate = Carbon::parse($date->end_dte);
    return array('sdate' => $sdate, 'edate' => $edate);

  }

  private function maxStartDate() {

    return Carbon::parse(ExtraData::where('empl_id', $this->empl_id)->where('element_id', $this->element_id)->max('start_dte'));

  }

  private function getActiveData() {

    $id = ExtraData::where('empl_id', $this->empl_id)->where('element_id', $this->element_id)->whereNull('end_dte')->orderBy('start_dte', 'desc')->first();

    if($id === null) {

      return false;

    } else {

      return $this->id = $id;

    }

  }

}
