<?php

namespace App\Services;

use App\Employee;
use App\IDM;
use Carbon\Carbon;
use Auth;

class IDManagement
{

  /**
   * New ID value
   * @var String
   */
  private $src_id;

  /**
   * ID Source Type ID
   * @var Integer
   */
  private $src_type_id;

  /**
   * Employee empl_id
   * @var Integer
   */
  private $empl_id;

  /**
   * Start Date of new ID
   * @var object Carbon\Carbon
   */
  private $startDate;

  /**
   * IDM model
   * @var object App\IDM
   */
  private $id;


  public function addID($empl_id, $src_id, $src_type_id, $startDate) {

    $this->empl_id = $empl_id;

    $this->src_id = $src_id;

    $this->src_type_id = $src_type_id;

    $this->startDate = Carbon::parse($startDate);

    $inactiveID = $this->getInactiveID();

    if($inactiveID !== false) {

      $date = $this->getDate();

      if($this->startDate->between($date['sdate'], $date['edate'])) {
          return "The ID you've attempted to map is between another agent's start and end date for the same ID. The ID was not mapped.";
      }
 
    }

    $activeID = $this->getActiveID();

    if($activeID !== false) {

      if($this->maxStartDate() >= $this->startDate) {
          return "The ID you've attempted to map has already been mapped to another agent in the past before the effective date. The ID was not mapped.";
      }

      $this->expireOldID();

    }

    $this->createNewID();

    return true;

  }

  private function createNewID() {

    $id = new IDM;

    $id->empl_id = $this->empl_id;

    $id->src_id = $this->src_id;

    $id->src_type_id = $this->src_type_id;

    $id->start_dte = $this->startDate->toDateString();

    $id->created_empl_id = Auth::User()->empl_id;

    $id->save();

  }

  private function maxStartDate() {

    return Carbon::parse(IDM::where('src_id', $this->src_id)->where('src_type_id', $this->src_type_id)->max('start_dte'));

  }

  private function getDate() {
    $currentid = $this->id;
    $date = IDM::where('id', $currentid)->first();
    $sdate = Carbon::parse($date->start_dte);
    $edate = Carbon::parse($date->end_dte);
    return array('sdate' => $sdate, 'edate' => $edate);

  }

  private function expireOldID() {

    $currentValue = $this->id;

    $currentValue->end_dte = $this->startDate->toDateString();

    $currentValue->save();

  }

  private function getActiveID() {

    $id = IDM::where('src_id', $this->src_id)->where('src_type_id', $this->src_type_id)->whereNull('end_dte')->orderBy('start_dte', 'desc')->first();

    if($id === null) {

      return false;

    } else {

      return $this->id = $id;

    }

  }

  private function getInactiveID() {

    $id = IDM::where('src_id', $this->src_id)->where('src_type_id', $this->src_type_id)->whereNotNull('end_dte')->orderBy('start_dte', 'desc')->first();

    if($id === null) {

      return false;

    } else {

      return $this->id = $id->id;

    }

  }

}
