<?php

namespace App\Services;

use App\Employee;
use App\ManagerHistory;
use Carbon\Carbon;
use Auth;

class Manager
{


  /**
   * Employee empl_id
   * @var Integer
   */
  private $empl_id;

  /**
   * Employees new manager_id
   * @var Integer
   */
  private $new_manager_id;

  /**
   * Employees current manager_id
   * @var Integer
   */
  private $manager_id;

  /**
   * Start Date of new Manager
   * @var object Carbon\Carbon
   */
  private $startDate;


  public function addManager($empl_id, $startDate, $new_manager_id, $manager_id) {

    $this->empl_id = $empl_id;

    $this->manager_id = $manager_id;

    $this->new_manager_id = $new_manager_id;

    $this->startDate = Carbon::parse($startDate);

    $this->expireCurrentManager();

    $this->updateEmployee();   

    return true;

  }

  private function createNewManager() {
    $today = Carbon::now();

    $id = new ManagerHistory;

    $id->empl_id = $this->empl_id;

    $id->manager_id = $this->new_manager_id;

    $id->start_dte = $this->startDate->toDateString();

    // TODO: Add logged in user's empl_id
    $id->created_empl_id = Auth::User()->empl_id;

    $id->created_at = $today->toDateTimeString();

    $id->updated_at = $today->toDateTimeString();

    $id->save();



  }



  private function expireCurrentManager() {
    $today = Carbon::now();

    $empl_id = $this->empl_id;

    $current_manager_id = $this->manager_id;

    $end_dte =  $this->startDate->toDateString();

    $updated_at = $today->toDateTimeString();

    $updated_empl_id = Auth::User()->empl_id;

    $empl_update = ManagerHistory::where('empl_id', $empl_id)->where('manager_id', $current_manager_id)->whereNull('end_dte')->first();

    if (!empty($empl_update))
      {
        $empl_update = ManagerHistory::select()
        ->where('empl_id', $empl_id)
        ->where('manager_id', $current_manager_id)
        ->whereNull('end_dte')
        ->update(['end_dte' => $end_dte, 'updated_at' => $updated_at, 'updated_empl_id' => $updated_empl_id]);

        $this->createNewManager();
      }

    else {

      $this->createNewManager();
    
    }


  }

    private function updateEmployee() {

    $new_manager_id = $this->new_manager_id;

    $empl_id = $this->empl_id;

    $ManagerName = Employee::where('empl_id', $new_manager_id)->first();

    $currentValue = Employee::where('empl_id', $empl_id)->first();

    $currentValue->manager_id = $this->new_manager_id;

    $currentValue->manager = $ManagerName->name;

    $currentValue->updated_at = $this->startDate->toDateTimeString();

    $currentValue->save();

  }


}
