<?php

namespace App\Services;

use App\Employee;
use App\Source;
use App\Elements;
use App\Lists;
use Carbon\Carbon;
use Auth;
use Excel;

class ImportTemplate
{



  public function TemplateIDM($row) {

    $file = Excel::create('file', function($excel) use ($row) {

        $excel->sheet('EmployeeTemplate', function($sheet) use ($row) {
          
            $sheet->SetCellValue("A1", "Employee ID");
            $sheet->SetCellValue("B1", "Employee ID Type");
            $sheet->SetCellValue("C1", "ID Source System");
            $sheet->SetCellValue("D1", "ID value");
            $sheet->SetCellValue("E1", "start_dte");
            $sheet->row(1, function($col) {

              $col->setBackground('#fd8924');
              $col->setFontColor('#ffffff');

            });

            if (isset($row)){
            $sheet->fromArray($row, null, 'A2', false, false);
            }


          for ($i = 2; $i <= 400; $i++)
          {
            if (empty($sheet->getCell('B'.$i)->getValue())){
               $sheet->SetCellValue("B".$i, "WIN ID"); 
            }
            $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('type');
          }

          for ($i = 2; $i <= 400; $i++)
          {
            $objValidation = $sheet->getCell('C'.$i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('source');
          }
        });

        $excel->sheet('sources', function($sheet2) {

              //TODO: Select from source and do loop for each cell
            $source = Source::select('source')->orderBy('source', 'asc')->get();
            $count = Source::select('source')->count();
            $row = 1;
            foreach ($source as $key => $value) {         
            $sheet2->SetCellValue("A".$row, $value['source']);
            $row++;
            }
            $sheet2->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                'source', $sheet2, 'A1:A'.$count
                )
            );

        });

        $excel->sheet('id_type', function($sheet3) {

            $sheet3->SetCellValue("A1", "WIN ID");
            $sheet3->SetCellValue("A2", "Synergy");
            $sheet3->SetCellValue("A3", "Employee ID");
            $sheet3->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                'type', $sheet3, 'A1:A3'
                )
            );

        });


            });

    return $file;

  }

  public function TemplateExtraData($row) {

    $file = Excel::create('file', function($excel) use ($row) {

        $excel->sheet('EmployeeTemplate', function($sheet) use ($row) {
          
            $sheet->SetCellValue("A1", "Employee ID");
            $sheet->SetCellValue("B1", "Employee ID Type");
            $sheet->SetCellValue("C1", "Element Name");
            $sheet->SetCellValue("D1", "Value");
            $sheet->SetCellValue("E1", "start_dte");
            $sheet->SetCellValue("F2", "If Job title is selected as Element Name, make sure the Value is selected from the dropdown list, otherwise you can type away the value.");
            $sheet->SetCellValue("G2", "Make sure that Birthdate value is in mm/dd/yyyy format.");
            $sheet->row(1, function($col) {

              $col->setBackground('#fd8924');
              $col->setFontColor('#ffffff');

            });


            if (isset($row)){
            $sheet->fromArray($row, null, 'A2', false, false);
            }


          for ($i = 2; $i <= 400; $i++)
          {
            if (empty($sheet->getCell('B'.$i)->getValue())){
               $sheet->SetCellValue("B".$i, "WIN ID"); 
            }
            $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('type');
          }


          for ($i = 2; $i <= 400; $i++)
          {
            $objValidation = $sheet->getCell('C'.$i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('=element');
          }


          for ($i = 2; $i <= 400; $i++)
          {
            $objValidation = $sheet->getCell('D'.$i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST );
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(false);
            $objValidation->setShowErrorMessage(false);
            $objValidation->setShowDropDown(true);
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('=INDIRECT($C$'.$i.')');
          }
        });



        $excel->sheet('elements', function($sheet2) {

              //TODO: Select from source and do loop for each cell
            $element = Elements::select('element_name')->orderBy('element_name', 'asc')->get();
            $count = Elements::select('element_name')->count();
            $row = 1;
            foreach ($element as $key => $value) {         
            $sheet2->SetCellValue("A".$row, $value['element_name']);
            $row++;
            }
            $sheet2->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                'element', $sheet2, 'A1:A'.$count
                )
            );

        });

        

        $excel->sheet('lists', function($sheet3) {

              //TODO: Select from source and do loop for each cell
            $list = Lists::select('list_name')->orderBy('list_name', 'asc')->get();
            $count = Lists::select('list_name')->count();
            $row = 1;
            foreach ($list as $key => $value) {         
            $sheet3->SetCellValue("A".$row, $value['list_name']);
            $row++;
            }
            $sheet3->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                'Job_Title', $sheet3, 'A1:A'.$count
                )
            );

        });

        $excel->sheet('id_type', function($sheet4) {

            $sheet4->SetCellValue("A1", "WIN ID");
            $sheet4->SetCellValue("A2", "Synergy");
            $sheet4->SetCellValue("A3", "Employee ID");
            $sheet4->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                'type', $sheet4, 'A1:A3'
                )
            );

        });

        });
    return $file;

  }

 

}
