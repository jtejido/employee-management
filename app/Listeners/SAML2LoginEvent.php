<?php
namespace App\Listeners;
use Redirect;
use Aacotroneo\Saml2\Events\Saml2LoginEvent as LoginEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SAML2LoginEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Saml2LoginEvent  $event
     * @return void
     */
    public function handle(LoginEvent $event)
    {

      $user = $event->getSaml2User();
      $userData = [
        'id' => $user->getUserId(),
        'attributes' => $user->getAttributes(),
        'assertion' => $user->getRawSamlAssertion()
      ];
      $laravelUser = $user->getUserId();//find user by ID or attribute
      //if it does not exist create it and go on  or show an error message
      \Auth::login($user);

    }
}
