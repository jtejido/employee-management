<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
    protected $table = 'list';

    protected $primaryKey = 'list_id';
}
