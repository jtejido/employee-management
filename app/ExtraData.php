<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraData extends Model
{

  protected $table = 'extra_data';

  public function elements()
  {
    return $this->hasOne('App\Elements', 'element_id', 'element_id');
  }
  public function employee()
  {
      return $this->belongsTo('App\Employee', 'empl_id', 'empl_id');
  }

}
