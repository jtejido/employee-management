<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elements extends Model
{
    protected $primaryKey = 'element_id';
}
