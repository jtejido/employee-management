<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;
use Auth;

class Employee extends Authenticatable
{

  protected $primaryKey = 'empl_id';

  public $timestamps = false;

  public function getRememberToken()
  {
    return null; // not supported
  }

  public function setRememberToken($value)
  {
    // not supported
  }

  public function getRememberTokenName()
  {
    return null; // not supported
  }

  public function scopeApplyACL($query)
  {

      if(Session::get('user.super') == true) {
        return $query;
      }

      if(Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('user.client'));
      }

      if(Session::has('user.site')) {
        $query->whereIn('site_id', Session::get('user.site'));
      }

      if(!Session::has('user.site') && !Session::has('user.client')) {
        return $query;
      }

      if (Session::has('search.filter')) {
        foreach (Session::get('search.filter') as $filter_temp){
          $filter = $filter_temp;
        }
        if($filter == "active") {
        $query->where('term_dte', '0000-00-00');
        }
        elseif($filter == "termed") {
        $query->where('term_dte', '!=', '0000-00-00');
        }
        elseif($filter == "newhire") {
        $query->where('hire_dte', '>', Carbon::now()->subDay(30));
        }
        elseif($filter == "all") {
        return $query;
        }
      }
      else {
        $query->where('term_dte', '0000-00-00');
      }

      return $query;

  }

    public function scopeApplyACLTermed($query)
  {

      $query->where('term_dte', '!=', '0000-00-00');

      if(Session::get('user.super') == true) {
        return $query;
      }

      if(Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('user.client'));
      }

      if(Session::has('user.site')) {
        $query->whereIn('site_id', Session::get('user.site'));
      }

      if(!Session::has('user.site') && !Session::has('user.client')) {
         return $query;
      }

      return $query;

  }


  /**
   * Overrides the method to ignore the remember token.
   */
  public function setAttribute($key, $value)
  {
    $isRememberTokenAttribute = $key == $this->getRememberTokenName();
    if (!$isRememberTokenAttribute) {
      parent::setAttribute($key, $value);
    }
  }
  public function getId()
  {
    return $this->id;
  }

  public function idm()
  {
    return $this->hasMany('App\IDM','empl_id','empl_id');
  }
  public function managerHistory()
  {
    return $this->hasMany('App\ManagerHistory','empl_id','empl_id');
  }
  public function acl()
  {
    return $this->hasMany('App\ACL','empl_id','empl_id');
  }
  public function extradata()
  {
    return $this->hasMany('App\ExtraData','empl_id','empl_id');
  }

  public function googleid()
  {
    return $this->hasMany('App\GoogleID','empl_id','empl_id');
  }

}
