<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleID extends Model
{
   protected $table = 'google_id_map';

  public function employee()
  {
      return $this->belongsTo('App\Employee', 'empl_id', 'empl_id');
  }
}
