<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ACL extends Model
{
  protected $table = 'acls';
  
  protected $primaryKey = 'acl_id';

  public function site()
  {
    return $this->hasOne('App\Site', 'site_id', 'site_id');
  }
  public function client()
  {
    return $this->hasOne('App\Client', 'client_id', 'client_id');
  }
  public function employee()
  {
    return $this->hasOne('App\Employee', 'empl_id', 'empl_id');
  }
  public function module()
  {
    return $this->hasOne('App\Module', 'module_id', 'module_id');
  }
}
