<?php

namespace App\DataTables;

use App\UnmappedIDMMapAll;
use Yajra\Datatables\Services\DataTable;
use Session;

class UnmappedIDMTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('ignore', function ($id) {
                return '<input type="checkbox" name="ids_to_ignore[]" value="'.$id->id.'" />';
            })
            ->addColumn('action', function ($id) {
                return '<a href="/empmgmt/unmapped_idm/'.$id->id.'/'.$id->src_type_id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Map ID</a>
                <a href="/empmgmt/unmapped_idm/'.$id->id.'/'.$id->src_type_id.'/ignore" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-trash"></i> Ignore</a>';
            })
            ->rawColumns(['ignore', 'action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = UnmappedIDMMapAll::ApplyACL()->select(['client', 'source', 'src_id', 'id', 'src_type_id', 'created_date']);
        if (Session::has('unmapped_idm.integration')) {
          if(Session::get('unmapped_idm.integration') != -1){
            $collection = $collection->where('src_type_id', Session::get('unmapped_idm.integration'));
          }
            $collection = $collection->get();
        }

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'ignore', 'name' => 'ignore', 'title' => '', 'orderable' => false, 'searchable' => false])
              ->addColumn(['data' => 'client', 'name' => 'client', 'title' => 'Client'])
              ->addColumn(['data' => 'source', 'name' => 'source', 'title' => 'Source'])
              ->addColumn(['data' => 'src_id', 'name' => 'src_id', 'title' => 'Source ID'])
              ->addColumn(['data' => 'created_date', 'name' => 'created_date', 'title' => 'Date Added'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false]);

    }
}
