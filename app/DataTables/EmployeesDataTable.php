<?php

namespace App\DataTables;

use App\Employee;
use App\EmployeeIDMap;
use Yajra\Datatables\Services\DataTable;
use Session;
use App\DataTables\Scopes\ACLScope;

class EmployeesDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('action', function ($user) {
                return '<a href="/empmgmt/employee/'.$user->empl_id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
            })->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {

        // below needs optimization
        if (Session::has('search.filter')) {
          foreach (Session::get('search.filter') as $filter_temp){
            $filter = $filter_temp;
          }
          if($filter == 'id') {
                $collection = EmployeeIDMap::select(['empl_id', 'username', 'name','manager', 'site', 'client'])->distinct('empl_id');
            }
            else {
                $collection = Employee::select(['empl_id', 'username', 'name','manager', 'site', 'client']);
            }
        }

        return $this->applyScopes($collection);

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'username', 'name' => 'username', 'title' => 'WIN ID'])
              ->addColumn(['data' => 'name', 'name' => 'Name', 'title' => 'Name'])
              ->addColumn(['data' => 'manager', 'name' => 'Manager', 'title' => 'Manager'])
              ->addColumn(['data' => 'site', 'name' => 'Site', 'title' => 'Location'])
              ->addColumn(['data' => 'client', 'name' => 'Client', 'title' => 'Client'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false]);

    }
}
