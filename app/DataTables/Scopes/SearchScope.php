<?php

namespace App\DataTables\Scopes;

use Yajra\Datatables\Contracts\DataTableScopeContract;
use Session;
use Carbon\Carbon;

class SearchScope implements DataTableScopeContract
{
    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {

      if (Session::has('search.client') && !Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('search.client'));
      } elseif (Session::has('search.client')) {
        $query->whereIn('client_id', array_intersect(Session::get('search.client'),Session::get('user.client')));
      } elseif(Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('user.client'));
      }

      if (Session::has('search.site') && !Session::has('user.site')) {
        $query->whereIn('site_id', Session::get('search.site'));
      } elseif (Session::has('search.site') && Session::has('user.site')) {
        $query->whereIn('site_id', array_intersect(Session::get('search.site'),Session::get('user.site')));
      } elseif(Session::has('user.site')) {
        $query->whereIn('site_id', Session::get('user.site'));
      }

      if (Session::has('search.name')) {
        // below needs optimization
        if (Session::has('search.filter')) {
          foreach (Session::get('search.filter') as $filter_temp){
            $filter = $filter_temp;
          }
          if($filter == 'id') {
            $query->where('src_id', 'LIKE', Session::get('search.name'));
          }
          else {
            $query->where('name', 'LIKE', Session::get('search.name'));
          }
        }
      }


      if (Session::has('search.status')) {
        foreach (Session::get('search.status') as $status_temp){
          $status = $status_temp;
        }
        if($status == "active") {
        $query->where('term_dte', '0000-00-00');
        }
        elseif($status == "termed") {
        $query->where('term_dte', '!=', '0000-00-00');
        }
        elseif($status == "newhire") {
        $query->where('hire_dte', '>', Carbon::now()->subDay(30));
        }
      }

        if(Session::get('search.newhire') == true) {
        $query->where('hire_dte', '>', Carbon::now()->subDay(30));

        if(Session::has('user.client')) {
          $query->whereIn('client_id', Session::get('user.client'));
        }

        if(Session::has('user.site')) {
          $query->whereIn('site_id', Session::get('user.site'));
        }
      }

    }
}
