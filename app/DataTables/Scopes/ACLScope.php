<?php

namespace App\DataTables\Scopes;

use Yajra\Datatables\Contracts\DataTableScopeContract;
use Session;

class ACLScope implements DataTableScopeContract
{
    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {

      if (Session::has('user.client')) {
        $query->whereIn('client_id', Session::get('user.client'));
      }

      if (Session::has('user.site')) {
        $query->whereIn('site_id', Session::get('user.site'));
      }

    }
}
