<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

  Route::group(['middleware' => ['auth',]], function () {
    Route::get('/', function () {
      return redirect()->route('overview');
    });
    Route::get('overview', 'OverviewController@overview')->name('overview');

    Route::post('unmapped_idm/bulk_ignore', 'UnmappedIDMController@bulkignore');
    Route::get('unmapped_idm', 'UnmappedIDMController@index');
    Route::get('unmapped_idm/{src_type_id}', 'UnmappedIDMController@view');
    Route::get('unmapped_idm/{src_type_id}/export', 'UnmappedIDMController@export');
    Route::get('unmapped_idm/{unmapped_id}/{src_type_id}/edit', 'UnmappedIDMController@edit');
    Route::get('unmapped_idm/{unmapped_id}/{src_type_id}/ignore', 'UnmappedIDMController@ignore');
    Route::post('unmapped_idm/{unmapped_id}/{src_type_id}/edit', 'UnmappedIDMController@save');

    Route::get('search', 'SearchController@index');
    Route::post('search', 'SearchController@search');
    Route::get('clearsearch', 'SearchController@forget');
    Route::post('search/name', 'SearchController@searchName');
    Route::get('search/newhire', 'SearchController@searchNewHire');

    Route::get('employees', 'EmployeesListingController@getBasic');
    Route::get('employees/{siteid}', 'EmployeesListingController@getBasic');
    Route::get('employee/{emplid}', 'EmployeeController@profile');
    Route::get('employee/{emplid}/team', 'EmployeeController@team');
    Route::post('access/add', 'ACLController@add');
    Route::get('access/remove/{acl_id}', 'ACLController@delete');

    Route::post('extradata/add', 'ElementController@addRedirect');

    Route::post('manager/add', 'ManagerController@addRedirect');
    Route::get('manager/search', 'ManagerController@search');

    Route::post('idm/add', 'IDMController@addRedirect');
    Route::get('export', 'ExportController@index');
    Route::get('export/employees/xls', 'ExportController@exportEmployeeList');
    Route::get('export/newhire/xls', 'ExportController@exportNewHireList');
    Route::get('export/assigned/xls', 'ExportController@exportAssignedID');
    Route::get('export/googleid/xls', 'ExportController@exportGoogleID');
    Route::get('export/missingids/{client_id}/xls', 'ExportController@exportMissingIDM');


    Route::get('import_idm', 'ImportIDMController@index');
    Route::get('import_idm/employees/download/{client_id}/client', 'ImportIDMController@downloadtemplateclient');
    Route::get('import_idm/employees/download/{site_id}/site', 'ImportIDMController@downloadtemplatesite');
    Route::get('import_idm/employees/download', 'ImportIDMController@downloadtemplate');
    Route::get('import_idm/employees/error', 'ImportIDMController@errortemplate');
    Route::post('import_idm/employees/upload', 'ImportIDMController@uploadtemplate');

    Route::get('import_extra_data', 'ImportExtraDataController@index');
    Route::get('import_extra_data/employees/download', 'ImportExtraDataController@downloadtemplate');
    Route::get('import_extra_data/employees/error', 'ImportExtraDataController@errortemplate');
    Route::post('import_extra_data/employees/upload', 'ImportExtraDataController@uploadtemplate');

    Route::resource('admin_elements/{element_id}/edit', 'ElementsController@edit');
    Route::delete('admin_elements/{element_id}/delete', 'ElementsController@destroy');
    Route::post('admin_elements/{element_id}/edit', 'ElementsController@save');
    Route::resource('admin_elements', 'ElementsController');

    Route::get('admin_users/{empl_id}/remove', 'AdminController@remove');
    Route::get('admin_users/search', 'AdminController@search');
    Route::resource('admin_users', 'AdminController');

    Route::get('admin_unmapped/{client_id}/{src_type_id}/edit', 'AdminUnmappedController@edit');
    Route::get('admin_unmapped/client/search', 'AdminUnmappedController@searchclient');
    Route::get('admin_unmapped/source/search', 'AdminUnmappedController@searchsource');
    Route::resource('admin_unmapped', 'AdminUnmappedController');
    Route::post('admin_unmapped/{client_id}/{src_type_id}/edit', 'AdminUnmappedController@save');

    Route::get('admin_list/search/{element_id}', 'ListController@search');
    Route::resource('admin_list/{element_id}/{list_id}/edit', 'ListController@edit');
    Route::post('admin_list/{element_id}/{list_id}/edit', 'ListController@save');
    Route::delete('admin_list/{element_id}/{list_id}/delete', 'ListController@destroy');
    Route::post('admin_list/{element_id}', 'ListController@store');
    Route::get('admin_list/{element_id}', 'ListController@index');

  });

  Route::get('login', 'LDAPAuthenticationController@index');
  Route::post('login', 'LDAPAuthenticationController@login');
  Route::get('logout', 'LDAPAuthenticationController@logout');

Route::get('/saml2/login', 'LDAPAuthenticationController@samlindex');
Route::get('/saml2/logout', 'LDAPAuthenticationController@samllogout');

