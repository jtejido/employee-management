const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
  mix.copy(
       'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
       'resources/assets/css'
   ).copy(
       'node_modules/datatables.net-buttons-bs/css',
       'resources/assets/css'
   ).copy(
       'node_modules/datatables.net-responsive-bs/css',
       'resources/assets/css'
   ).copy(
       'node_modules/datatables.net-scroller-bs/css',
       'resources/assets/css'
   );
    mix.sass('app.scss')
        .styles([
          'dataTables.bootstrap.css',
          'buttons.bootstrap.css',
          'responsive.bootstrap.css',
          'scroller.bootstrap.css',
        ], 'public/css/lib.css')
       .copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts')
       .webpack('app.js');
});
