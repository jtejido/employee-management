@extends('layouts.app')
@section('title', 'Import IDM')
@section('content')
    <h1 class="page-header">Bulk Import IDM</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>The following import template is available to bulk import employees' data into the system.</p>
    <div class="row">
    <div class="col-md-2">
    <a class="btn btn-primary" href="/employees/public/import_idm/employees/download">Download Template</a>
    </div>
    <div class="col-md-2">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle client" type="button" data-toggle="dropdown">Select Client
      <span class="caret"></span></button>
      <ul class="dropdown-menu scrollable-menu collapse" id="client">
            @foreach($clients as $client)
                <li><a href="/employees/public/import_idm/employees/download/{{ $client->client_id }}/client">{{ $client->client }}</a></li>
            @endforeach
      </ul>
    </div>
    </div>
    <div class="col-md-2">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle site" type="button" data-toggle="dropdown">Select Site
      <span class="caret"></span></button>
      <ul class="dropdown-menu scrollable-menu collapse" id="site">
            @foreach($sites as $site)
                <li><a href="/employees/public/import_idm/employees/download/{{ $site->site_id }}/site">{{ $site->site }}</a></li>
            @endforeach
      </ul>
    </div>
    </div>
    <br><br>
    <div class="col-sm-6">
        <form action="/employees/public/import_idm/employees/upload" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
          <div class="form-group">
            <label class="control-label" for="InputFile">Import file</label>
            <input type="file" name="upload" id="input-1a" type="file" class="file" data-show-preview="false">
          </div>
        </form>
    </div>
    </div>
    <br><br>
@if (session('import_idm.row'))

    <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>Employee ID</th>
                <th>Employee ID Type</th>
                <th>ID Source System</th>
                <th>ID value</th>
                <th>Start Date</th>
                <th>Error Message</th>
              </tr>
            </thead>
            <tbody>
              @foreach(session('import_idm.row') as $key => $value)
              <tr>
                <td>{{ $value['id'] }}</td>
                <td>{{ $value['id_type'] }}</td>
                <td>{{ $value['id_source_system'] }}</td>
                <td>{{ $value['id_value'] }}</td>
                <td>{{ $value['start_dte'] }}</td>
                <td>{{ $value['error'] }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <br>
    <a class="btn btn-primary" href="/employees/public/import_idm/employees/error">Download table</a>
@endif
@endsection
@section('pagejs')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});


</script>
@endsection