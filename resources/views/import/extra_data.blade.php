@extends('layouts.app')
@section('title', 'Import Extra Data')
@section('content')
    <h1 class="page-header">Bulk Import Extra Data</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>The following import template is available to bulk import employees' data into the system.</p>
    <div class="row">
    <div class="col-md-5">
    <a class="btn btn-primary" href="/empmgmt/import_extra_data/employees/download">Download Template</a>
    </div>
    <br><br>
    <div class="col-sm-6">
        <form action="/empmgmt/import_extra_data/employees/upload" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
          <div class="form-group">
            <label class="control-label" for="InputFile">Import file</label>
            <input type="file" name="upload" id="input-1a" type="file" class="file" data-show-preview="false">
          </div>
        </form>
    </div>
    </div>
    <br><br>
@if (session('import_extra_data.row'))

    <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>Employee ID</th>
                <th>Employee ID Type</th>
                <th>Element Name</th>
                <th>Element Value</th>
                <th>Start Date</th>
                <th>Error Message</th>
              </tr>
            </thead>
            <tbody>
              @foreach(session('import_extra_data.row') as $key => $value)
              <tr>
                <td>{{ $value['id'] }}</td>
                <td>{{ $value['id_type'] }}</td>
                <td>{{ $value['element_name'] }}</td>
                <td>{{ $value['element_value'] }}</td>
                <td>{{ $value['start_dte'] }}</td>
                <td>{{ $value['error'] }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <br>
    <a class="btn btn-primary" href="/empmgmt/import_extra_data/employees/error">Download table</a>
@endif
@endsection
@section('pagejs')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});
</script>
@endsection