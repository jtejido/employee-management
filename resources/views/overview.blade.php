@extends('layouts.app')

@section('title', 'Overview')

@section('content')
    <h1 class="page-header">Dashboard</h1>

    <div class="row placeholders">
      <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
          <div class="panel-body">
            <h1>{{ $employees }}</h1>
          </div>
          <div class="panel-footer">Active Employees</div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
          <div class="panel-body">
            <h1><a href="/empmgmt/search/newhire">{{ $new_hires }}</a></h1>
          </div>
          <div class="panel-footer">New Hires</div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
          <div class="panel-body">
            <h1>{{ $unmapped_ids }}</h1>
          </div>
          <div class="panel-footer">Unmapped IDs</div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-3 placeholder">
        <div class="panel panel-default">
          <div class="panel-body">
            <h1>{{ $terminated }}</h1>
          </div>
          <div class="panel-footer">Terminated Employees</div>
        </div>
      </div>
    </div>

    <h2 class="sub-header">Your Access</h2>
    <p>The following list is of your accounts and locations you have access to manage data for.</p>
    <p>If you need additional access, please raise a help desk ticket.</p>
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Client</th>
            <th>Site</th>
            <th>Changed On</th>
          </tr>
        </thead>
        <tbody>
          @foreach($acls as $access)
          <tr>
            <td>{{ $access->client->client or 'All Clients'}}</td>
            <td>{{ $access->site->name or 'All Sites'}}</td>
            <td>{{ $access->updated_at }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
@endsection
