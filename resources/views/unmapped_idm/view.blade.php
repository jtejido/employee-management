@extends('layouts.app')
@section('title', 'Unmapped IDs')
@section('content')
    <h1 class="page-header">Unmapped IDs</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to view and manage Unmapped IDs. Please select the Integration or Source System below to filter results.</p>
    <br><br>
    <div class="row">

    <div class="col-md-2">
    <div class="dropdown">
      <button class="btn btn-default dropdown-toggle client" type="button" data-toggle="dropdown">Select Integration
      <span class="caret"></span></button>
      <ul class="dropdown-menu scrollable-menu collapse" id="client">
            <li><a href="/empmgmt/unmapped_idm/-1">All</a></li>
            @foreach($integrations as $integration)
                <li><a href="/empmgmt/unmapped_idm/{{$integration->src_type_id}}">{{ $integration->source }}</a></li>
            @endforeach
      </ul>
    </div>
    </div>
    <div class="col-md-1">
      <a  class="btn btn-default" href="/empmgmt/unmapped_idm/{{ $src_type_id }}/export">Export</a>
    </div>
    </div>
    <br>
<form method="post" action="/empmgmt/unmapped_idm/bulk_ignore">
    {!! csrf_field() !!}
     <div class="row">
    <div class="col-md-2">
    <button type="submit" class="btn btn-primary">Ignore Selected</button>
    </div>
    </div>
    <br>
 {!! $html->table(['class' => 'table table-bordered']) !!}
</form>

@endsection
@section('pagejs')
  <script type="text/javascript">
    $.extend(true, $.fn.dataTable.defaults, {
        language: {
          "search": "Filter records:"
        }
    });
  </script>
{!! $html->scripts() !!}
@endsection