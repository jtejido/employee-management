@extends('layouts.app')
@section('title', 'Unmapped IDs')
@section('content')
@section('pagejs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$( function() {
    $("#employee_name").autocomplete({
      scroll: true,
      autoFocus: true,
      minLength: 3,
      source: function( request, response ) {
        $.ajax({
          url: "/empmgmt/manager/search",
          dataType: "json",
          data: {
              searchText: request.term
          },
          success: function( data ) {
           response($.map(data, function(item) {
                return {
                    label: item.name,
                    value: item.id
                };
            }));
          }
        });
      },
      select: function( event, ui) {
        $(this).val(ui.item.label);
        $("#employee_id").val(ui.item.value);
        return false;
      }
} );
} );
</script>
@endsection
    <h1 class="page-header">Unmapped IDs</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to map IDs to Specific Employees.</p>
    <br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <form class="form-inline" method="POST">
                <p>You can map the specific ID from here. All changes are recorded.</p>

                <table class="table table-bordered">
                  <thead>
                    <th>Source ID</th>
                    <th>Source</th>
                    <th>Employee's Name</th>
                    <th>Effective Date</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{{ $unmapped_id->src_id }}</td>
                      <td>{{ $unmapped_id->source }}</td>
                      <td>
                      <div class="ui-widget">
                        <input class="form-control" id="employee_name" name="employee_name" placeholder="Type a Name">
                        <input type="hidden" id="employee_id" name="employee_id">
                      </div>
                      </td>
                      <td>
                          <div class="input-group date" data-provide="datepicker">
                            <input type="text" name="start_dte" class="form-control datepicker" format="yyyy-MM-dd" data-date-end-date="0d">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>

                {{ csrf_field() }}
                <input type="hidden" value="{{$unmapped_id->src_type_id}}" name="src_type_id">
                <input type="hidden" value="{{$unmapped_id->src_id}}" name="src_id">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="/empmgmt/unmapped_idm/{{$src_type_id}}" class="btn btn-primary">Back</a>
              </form>
            </div>
          </div>
        </div>  
@endsection