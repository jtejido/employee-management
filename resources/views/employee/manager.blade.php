@section('pagejs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$( function() {
    $("#new_manager").autocomplete({
      scroll: true,
      autoFocus: true,
      minLength: 3,
      source: function( request, response ) {
        $.ajax({
          url: "/empmgmt/manager/search",
          dataType: "json",
          data: {
              searchText: request.term
          },
          success: function( data ) {
           response($.map(data, function(item) {
                return {
                    label: item.name,
                    value: item.id
                };
            }));
          }
        });
      },
      select: function( event, ui) {
        $(this).val(ui.item.label);
        $("#new_manager_id").val(ui.item.value);
        return false;
      }
} );
} );
</script>
@endsection
<div class="panel panel-default">
      <div class="panel-heading" role="tab" id="employeePanelHeaderManager">
        <h4 class="panel-title">
          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#employeePanelManager" aria-expanded="false" aria-controls="employeePanelManager">
            Manager
          </a>
        </h4>
      </div>
      <div id="employeePanelManager" class="panel-collapse collapse" role="tabpanel" aria-labelledby="employeePanelHeaderManager">
        <div class="panel-body">
          <p>You can change this employee's manager by selecting a new manager from the box and entering an effective date. Please note, this does not update local HRIS. Your local HRIS system should be updated with your change.</p>

          <form class="form-inline" method="POST" action="/empmgmt/manager/add">
            <div class="form-group">
            <div class="ui-widget">
              <label for="new_manager">New Manager</label>
              <input class="form-control" id="new_manager" name="new_manager" placeholder="Type a Name">
              <input type="hidden" id="new_manager_id" name="new_manager_id">
            </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail2">Effective Date</label>
              <div class="input-group date" data-provide="datepicker">
            <input type="text" name="start_dte" class="form-control datepickermanager" format="yyyy-MM-dd" data-date-end-date="0d">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
            </div>
            </div>
            <input type="hidden" name="manager_id" value="{{ $employee->manager_id }}">
            <input type="hidden" name="empl_id" value="{{ $employee->empl_id }}">
            
             {{ csrf_field() }}
            <button type="submit" class="btn btn-default">Update</button>
          </form>
            <br/>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Manager</th>
                <th>Date Range</th>
                <th>Changed On</th>
              </tr>
            </thead>
            <tbody>
              @foreach($employee->managerHistory as $manager)
                @if($manager->end_dte)
                  <tr class="active">
                @else
                  <tr>
                @endif
                <td>{{ $manager->manager_employee->name }}</td>
                <td>{{ $manager->start_dte }} - {{ $manager->end_dte or 'now' }}</td>
                <td>{{ $manager->updated_at }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
