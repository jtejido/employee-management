@extends('layouts.app')

@section('title', 'Team')

@section('content')

<h2 class="page-header">View Employees</h2>
<p>The employees listed here are the employees you have access to.</p>
    <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>WIN ID</th>
                <th>Name</th>
                <th>Manager</th>
                <th>Location</th>
                <th>Client</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($employees as $key => $value)
              <tr>
                <td>{{ $value->username }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->manager }}</td>
                <td>{{ $value->site }}</td>
                <td>{{ $value->client }}</td>
                <td><a href="/empmgmt/employee/{{$value->empl_id}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>

@endsection

@section('pagejs')
<script type="text/javascript">
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});
</script>
@endsection
