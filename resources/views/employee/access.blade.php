<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="employeePanelHeaderAccess">
    <h4 class="panel-title">
      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#employeePanelAccess" aria-expanded="false" aria-controls="employeePanelAccess">
        Access Management
      </a>
    </h4>
  </div>
  <div id="employeePanelAccess" class="panel-collapse collapse" role="tabpanel" aria-labelledby="employeePanelHeaderAccess">
    <div class="panel-body">

      <form class="form-inline" method="POST" action="/empmgmt/access/add">
        <div class="form-group">
          <label for="exampleInputEmail2">Site</label>
          <select class="form-control" name="site_id">
            <option value="0">All Sites</option>
            @foreach($sites as $site)
              <option value="{{ $site->site_id }}">{{ $site->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail2">Client</label>
          <select class="form-control" name="client_id">
            <option value="0">All Clients</option>
            @foreach($clients as $client)
              <option value="{{ $client->client_id }}">{{ $client->client }}</option>
            @endforeach
          </select>
        </div>
        <input type="hidden" name="empl_id" value="{{ $employee->empl_id }}">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default">Add</button>
      </form>
      <br/>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Site</th>
            <th>Client</th>
            <th>Changed On</th>
            <th>Remove</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employee->acl as $access)
          <tr>
            <td>{{ $access->site->name or 'All Sites'}}</td>
            <td>{{ $access->client->client or 'All Clients'}}</td>
            <td>{{ $access->updated_at }}</td>
            <td><a href="/empmgmt/access/remove/{{ $access->acl_id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
