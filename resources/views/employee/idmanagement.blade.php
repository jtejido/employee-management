<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="employeePanelHeaderIDAssignment">
    <h4 class="panel-title">
      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#employeePanelIDAssignment" aria-expanded="false" aria-controls="employeePanelIDAssignment">
        ID Assignments
      </a>
    </h4>
  </div>
  <div id="employeePanelIDAssignment" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="employeePanelHeaderIDAssignment">
    <div class="panel-body">
      <p>You can add or change ID Assignments for employees here. IDs assigned to employees will be used to map ACD, WFM, HRIS, and other data sources together as required.</p>
      <p>Your account will have access to edit specific ID entries for employees. If you are not able to add an ID entry, please contact support for assistance.</p>

      <form class="form-inline" method="POST" action="/empmgmt/idm/add">
        <div class="form-group">
          <label for="exampleInputName2">ID Entry</label>
          <input type="text" class="form-control" id="managerID" name="src_id" placeholder="Add a ID Value">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail2">Source System</label>
          <select class="form-control" name="src_type_id">
            @foreach($sources as $source)
              <option value="{{ $source->src_type_id }}">{{ $source->source }}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail2">Effective Date</label>
          <div class="input-group date" data-provide="datepicker">
            <input type="text" name="start_dte" class="form-control datepicker" format="yyyy-MM-dd" data-date-end-date="0d">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        </div>
        <input type="hidden" name="empl_id" value="{{ $employee->empl_id }}">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default">Update</button>
      </form>
      <br/>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>System</th>
            <th>Login</th>
            <th>Date Range</th>
            <th>Changed On</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employee->idm as $idm)
          <tr>
            <td>{{ $idm->source->source }}</td>
            <td>{{ $idm->src_id }}</td>
            <td>{{ $idm->start_dte }} - {{ $idm->end_dte or 'now' }}</td>
            <td>{{ $idm->updated_at }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
