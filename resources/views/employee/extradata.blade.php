<div class="panel panel-default">
  <div class="panel-heading" role="tab" id="employeePanelHeaderEmployeeData">
    <h4 class="panel-title">
      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#employeePanelEmployeeData" aria-expanded="false" aria-controls="employeePanelEmployeeData">
        Employee Data
      </a>
    </h4>
  </div>
  <div id="employeePanelEmployeeData" class="panel-collapse collapse" role="tabpanel" aria-labelledby="employeePanelHeaderEmployeeData">
    <div class="panel-body">
    <p>You can add some employee's extra data by selecting an Element Name, a correspondng value and an effective date. Please note, this does not update local HRIS. Your local HRIS system should be updated with your change.</p>

          <form class="form-inline" method="POST" action="/empmgmt/extradata/add">
            <div class="form-group">
              <label for="exampleInputEmail2">Element Name</label>
              <select class="form-control" name="element_id" id="element_name">
                @foreach($elements as $element)
                  <option value="{{ $element->element_id }}">{{ $element->element_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputName2">Element Value</label>
              <input type="text" class="form-control" id="element_value" name="value" placeholder="Add an Element Value">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail2">Effective Date</label>
              <div class="input-group date" data-provide="datepicker">
            <input type="text" name="start_dte" class="form-control datepickerelement" format="yyyy-MM-dd" data-date-end-date="0d">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
            </div>
            </div>
            <input type="hidden" name="empl_id" value="{{ $employee->empl_id }}">
             {{ csrf_field() }}
            <button type="submit" class="btn btn-default">Update</button>
          </form>
      <br/>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Element Name</th>
            <th>Value</th>
            <th>Date Range</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employee->extradata as $extradata)
          <tr>
            <td>{{ $extradata->elements->element_name }}</td>
            <td>{{ $extradata->value }}</td>
            <td>{{ $extradata->start_dte }} - {{ $extradata->end_dte or 'now' }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@push('js')
<script type="text/javascript">
<?php
echo "var javascript_array = ". $list . ";\n";
?>
$("#element_name").on('change', function(){
  var id_input = $('#element_name').val();
  if(id_input == 2){
        $("#element_value").datepicker();
    }
  else if(jQuery.inArray(parseInt(id_input), javascript_array) != -1){
        $("#element_value").datepicker('destroy');
            $( function() {
                  $("#element_value").autocomplete({
                    scroll: true,
                    autoFocus: true,
                    minLength: 0,
                    source: function( request, response ) {
                      $.ajax({
                        url: "/empmgmt/admin_list/search/"+id_input,
                        dataType: "json",
                        data: {
                            searchText: request.term
                        },
                        success: function( data ) {
                         response($.map(data, function(item) {
                              return {
                                  label: item.name,
                                  value: item.id
                              };
                          }));
                        }
                      });
                    },
                    select: function( event, ui) {
                      $(this).val(ui.item.label);
                      return false;
                    }
              } )
              .focus(function() {
                  $(this).autocomplete('search', "")
              });
              } );
    }
  else {
      $('#element_value').datepicker('destroy');
    }
});
</script>
@endpush
