@extends('layouts.app')

@section('title', 'Profile')

@section('content')
        <p class="lead">You are editing this employee's profile</p>
        <p>Changes to this employee's profile will reflect in reporting systems during the next sync. If you need to change mapping information, make the adjustment below.</p>
  <div class="row">

    <div class="col-md-5">
      <ul class="list-group">
        <li class="list-group-item active orange"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <b>{{ $employee->name }}</b></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-equalizer" aria-hidden="true"></span> Win ID is <b>{{ $employee->username }}</b></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Hired on <b>{{ Carbon::parse($employee->hire_dte)->toFormattedDateString() }}</b></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Working on behalf of <b>{{ $employee->client }}</b></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Located in <b>{{ $employee->site }}</b></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></span> Reports to <b><a href="/empmgmt/employee/{{$employee->manager_id}}">{{ $employee->manager }}</a></b></li>
        <li class="list-group-item"><span class="glyphicon glyphicon-bell"></span> Go to <a href="/empmgmt/employee/{{$employee->empl_id}}/team"><b>Team</b></a></li>
      </ul>
    </div>
    <div class="col-md-7">
      <ul class="list-group">
      @if (!empty($id_assigned))
        @foreach ($id_assigned as $key => $value)
          @if (!empty($value->src_id))
          <li class="list-group-item"><span style="color: green;" class="glyphicon glyphicon-ok" aria-hidden="true"></span> <b>{{ $value->source }}</b> is already added.</li>
          @else
          <li class="list-group-item"><span style="color: red;" class="glyphicon glyphicon-remove" aria-hidden="true"></span> <b>{{ $value->source }}</b> is still missing.</li>
          @endif
        @endforeach
      @endif
      </ul>
    </div>
  </div>

  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @include('employee.idmanagement')
    @if(Session::get('user.super') == true)
      @include('employee.access')
    @endif
    @include('employee.extradata')
    @include('employee.manager')
  </div>

@endsection