<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee Management @yield('title')</title>
    <link href="/empmgmt/css/app.css" rel="stylesheet">
    <link href="/empmgmt/css/lib.css" rel="stylesheet">
    <link rel="stylesheet" href="/empmgmt/css/jquery-ui.css">
    <link rel="stylesheet" href="/empmgmt/css/fileinput.min.css">
    <link rel="stylesheet" href="/empmgmt/css/multi-select.css">

    <!-- TODO: load these in CNET -->
    <!--[if lt IE 9]>
      <script src="/empmgmt/js/html5shiv.min.js"></script>
      <script src="/empmgmt/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="#"><img src="/empmgmt/img/conduent-logo.svg" /> Employee Management</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          @if(Auth::check())
          @if(!(Request::is('search')))
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/empmgmt/search">Advanced Search</a></li>
            <li><a href="https://mystats.services.xerox.com/support">Help</a></li>
            <li><a href="/empmgmt/logout">Logout</a></li>
          </ul>
          <form class="navbar-form navbar-right form-inline" method="POST" action="/empmgmt/search/name">
            <select class="form-control" name="filter">
            <option value="name">Name</option>
            <option value="id">ID</option>
            </select>
            <select class="form-control" name="status">
            <option value="active">Active Employees</option>
            <option value="termed">Inactive Employees</option>
            <option value="newhire">New Hires</option>
            <option value="all">All</option>
            </select>
            <input type="text" class="form-control" name="name" placeholder="Search...">
            {!! csrf_field() !!}
          </form>
          @endif
          @endif

        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          @if(Auth::check())
          <ul class="nav nav-sidebar">
            <li {{((Request::is('overview')) ? 'class=active' : '')}}><a href="/empmgmt/overview">Overview</a></li>
            <li {{((Request::is('export')) ? 'class=active' : '')}}><a href="/empmgmt/export">Export</a></li>
            <li {{((Request::is('unmapped_idm')) ? 'class=active' : '')}}><a href="/empmgmt/unmapped_idm">Unmapped IDs</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li {{((Request::is('import_idm')) || (Request::is('import_extra_data')) ? 'class=active' : '')}}><a href="#" class="bulkimport">Bulk Import <b class="pull-right">&rsaquo;</b></a></li>
            <ul class="collapse nav" id="bulkimport" style="list-style-type:none;margin-left:26px !important;">
            <li {{((Request::is('import_idm')) ? 'class=active' : '')}}><a href="/empmgmt/import_idm">Bulk Import IDM</a></li>
            <li {{((Request::is('import_extra_data')) ? 'class=active' : '')}}><a href="/empmgmt/import_extra_data">Bulk Import Extra Data</a></li>
            </ul>
          </ul>
          @if(Session::get('user.super') == true)
          <ul class="nav nav-sidebar">
            <li {{((Request::is('admin_elements')) || (Request::is('admin_users')) ? 'class=active' : '')}}><a href="#" class="admin">Admin <b class="pull-right">&rsaquo;</b></a></li>
            <ul class="collapse nav" id="admin" style="list-style-type:none;margin-left:26px !important;">
            <li {{((Request::is('admin_users')) ? 'class=active' : '')}}><a href="/empmgmt/admin_users">Admin Users</a></li>
            <li {{((Request::is('admin_elements')) ? 'class=active' : '')}}><a href="/empmgmt/admin_elements">Elements</a></li>
            <li {{((Request::is('admin_unmapped')) ? 'class=active' : '')}}><a href="/empmgmt/admin_unmapped">Admin Source Systems</a></li>
            </ul>
          </ul>
          @endif
          <ul class="nav nav-sidebar">
            <li {{((Request::is('employees')) ? 'class=active' : '')}}><a href="/empmgmt/employees">Employees</a></li>
          </ul>
        @endif
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          @if (session('status'))
              <div class="alert alert-info">
                  {{ session('status') }}
              </div>
          @endif
          @yield('content')
        </div>
      </div>

    </div>
    
    <script src="/empmgmt/js/app.js"></script>
    <script src="/empmgmt/js/fileinput.min.js"></script>
    <script src="/empmgmt/js/jquery.multi-select.js"></script>
    @yield('pagejs')
    @stack('js')
    <script>
    $(document).ready(function(){
      $(".bulkimport").click(function(e){
          e.preventDefault();
          $("#bulkimport").collapse('toggle');
      });
      $(".admin").click(function(e){
          e.preventDefault();
          $("#admin").collapse('toggle');
      });
    });

    $(document).ready(function() {
      $(".dropdown-toggle").dropdown();
    });
    
    $('#clients').multiSelect();
    $('#sources').multiSelect();

  </script>
  </body>
</html>
