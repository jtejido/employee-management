@extends('layouts.app')

@section('title', 'Exports')

@section('content')
    <h1 class="page-header">Exports</h1>

    <p>The following exports are available to export data based on your user access.</p>
    <p>If you need additional access, please raise a help desk ticket.</p>
    <div class="row">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Export</th>
            <th>Report Name</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a class="btn btn-primary" href="/empmgmt/export/employees/xls">Export</a></td>
            <td>Employees Details</td>
            <td>Export information about employees active configuration.</td>
          </tr>
          <tr>
            <td><a class="btn btn-primary" href="/empmgmt/export/newhire/xls">Export</a></td>
            <td>New Hire Employees Details</td>
            <td>Export information about employees' active configuration hired within 30 days.</td>
          </tr>
          <tr>
            <td><a class="btn btn-primary" href="/empmgmt/export/assigned/xls">Export</a></td>
            <td>IDs Assigned to Users</td>
            <td>Export information about ID's assigned to users.</td>
          </tr>
          <tr>
            <td><a class="btn btn-primary" href="/empmgmt/export/googleid/xls">Export</a></td>
            <td>Google IDs Assigned to Users</td>
            <td>Export information about Google ID's assigned to users.</td>
          </tr>
            <tr>
            <td style="height: 30px;">
                <div class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Client
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu scrollable-menu collapse" id="client">
                        <li><a href="/empmgmt/export/missingids/-1/xls">All</a></li>
                        @foreach($clients as $client)
                          <li><a href="/empmgmt/export/missingids/{{$client->client_id}}/xls">{{ $client->client }}</a></li>
                        @endforeach
                  </ul>
                </div>
            </td>
            <td>Missing IDs</td>
            <td>Export information about Missing IDs for each employees from a selected Client.</td>
          </tr>
        </tbody>
      </table>
    </div>
@endsection
