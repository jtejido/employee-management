@extends('layouts.app')

@section('title', 'Search')
@section('content')
  <h2 class="page-header">Advanced Search</h2>
  <p>Search for employees using specific conditions.</p>

      <form method="POST">
        {!! csrf_field() !!}
        <div class="form-group form-inline">
            <form class="navbar-form navbar-right form-inline" method="POST" action="/employees/public/search/name">
            <select class="form-control" name="filter">
            <option value="name">Name</option>
            <option value="id">ID</option>
            </select>
            <select class="form-control" name="status">
            <option value="active">Active Employees</option>
            <option value="termed">Inactive Employees</option>
            <option value="newhire">New Hires</option>
            <option value="all">All</option>
            </select>
            <input type="text" class="form-control" name="name" placeholder="Search...">
        <button class="btn btn-success" type="submit">Search</button>
      </div>
      <div class="form-group">
        <label>Account / Client Name</label>
        <div class="row">
        @foreach($clients as $client)
          <div class="col-md-2">
            <input type="checkbox" name="client[]" value="{{ $client->client_id }}"> {{ $client->client }}
          </div>
        @endforeach
        </div>
      </div>
      <div class="form-group">
        <label>Location Name</label>
        <div class="row">
        @foreach($sites as $site)
          <div class="col-md-2">
            <input type="checkbox" name="site[]" value="{{ $site->site_id }}"> {{ $site->site }}
          </div>
        @endforeach
        </div>      
        </div>
      
      </form>
@endsection
