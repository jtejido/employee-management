@extends('layouts.app')

@section('title', 'Login')
@section('content')

  <h3 class="">Employee Management</h3>
  <p class="lead">This tool is for managing Conduent employee information, including ID Mapping relationships, hiearchy, and extra mapping information for reporting purposes.</p>
  <Div class="row">
    <div class="col-md-6">
<div class="panel panel-default">
    <div class="panel-body">
      <p>Login using your <b>Conduent</b> WIN ID and Password.</p>
      <form class="text-center" action="" method="POST">
        {!! csrf_field() !!}

        @if (null !== Session::get('error'))
          <div class="alert alert-danger" role="alert">{!! Session::get('error') !!}</div>
        @endif
        @if (null !== Session::get('info'))
          <div class="alert alert-info" role="alert">{!! Session::get('info') !!}</div>
        @endif
        <div class="form-group">

        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" class="form-control" name="username" id="inputUsername" class="" placeholder="Username" required autofocus>
      </div>
      <div class="form-group">

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" class="form-control" name="password" id="inputPassword" class="" placeholder="Password" required>
      </div>
      <div class="form-group">

        <button class="btn btn-success" type="submit">Login</button>
        <a type="button" class="btn btn-info" href="https://mystats.services.xerox.com/support">Get Help</a>
      </div>
      </form>

    </div>
    <div class="panel-footer">To change your password, visit <a href="http://aim.acs-inc.com/sspr">SSPR</a></div>

  </div>
</div>
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-body">
You have accessed the Condeunt Employee Management system. It is for use only by Conduent employees and representatives expressly authorized by Conduent to use this site. If you are not an authorized user, please exit immediately. 
The information contained at this site is confidential and proprietary information of Conduent and is to be used only for maintanace of employees for which the user is responsible. Any other use is prohibited. Do not share your password with anyone else.

    </div>
  </div>

</div>
</div>
@endsection
