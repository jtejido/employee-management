@extends('layouts.app')
@section('title', 'Elements Administration')
@section('content')
    <h1 class="page-header">Elements Administration</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to add, edit or remove Element items shown to users who are attempting to add Elements to another user's Profile page.</p>
    <div class="row">
    <div class="col-md-5">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-element">Add Element</button>
    </div>
    </div>
    <br><br>
    <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>Element Name</th>
                <th>Is Element a List?</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($elements as $key => $value)
              <tr>
                <td>{{ $value->element_name }}</td>
                <td>
                @if($value->is_list == 1)
                Yes
                @else
                No
                @endif
                </td>
                <td>{{ $value->created_at }}</td>
                <td>{{ $value->updated_at }}</td>
                <td>
                <form method="post" action="/empmgmt/admin_elements/{{ $value->element_id }}/delete">
                {!! csrf_field() !!}
                <a href="/empmgmt/admin_elements/{{ $value->element_id }}/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                
                <button type="submit" class="btn btn-xs btn-primary btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</button>

                @if($value->is_list == 1)
                <a href="/empmgmt/admin_list/{{ $value->element_id }}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Manage List</a>
                @endif
                
                <input name="_method" value="DELETE" type="hidden">
                </form>
                </td>
              </tr>
              @endforeach
            </tbody>
    </table>
<div id="create-element" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Element Information</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="/empmgmt/admin_elements"> 
                        {!! csrf_field() !!}
                                    <div class="row">
                                        <label>Element Name</label>
                                        <input class="form-control" name="element_name" type="text" placeholder="Please type the Element Name here">
                                        <br />
                                        <label>Is this a List?</label>
                                        <select class="form-control" name="is_list">
                                        <option></option>
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                        </select>
                                    </div><br />
                            <button class="button default" type="submit"> Copy</button>
                        </form>  
                    </div>
            </div>
</div>
</div>
</div>

@endsection
@section('pagejs')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});
</script>
@endsection