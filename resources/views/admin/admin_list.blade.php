@extends('layouts.app')
@section('title', 'List Administration')
@section('content')
    <h1 class="page-header">List Administration</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to add, edit or remove items from the list shown to users who are attempting to add it to another user's Profile page.</p>
    <div class="row">
    <div class="col-md-5">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-list">Add Item</button>
    </div>
    </div>
    <br><br>
    <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>List Name</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $key => $value)
              <tr>
                <td>{{ $value->list_name }}</td>
                <td>{{ $value->created_at }}</td>
                <td>{{ $value->updated_at }}</td>
                <td>
                <form method="post" action="/empmgmt/admin_list/{{ $value->element_id }}/{{ $value->list_id }}/delete"> 
                {!! csrf_field() !!}
                <a href="/empmgmt/admin_list/{{ $value->element_id }}/{{ $value->list_id }}/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-xs btn-primary btn-sm"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                <input name="_method" value="DELETE" type="hidden">
                </form>
                </td>
              </tr>
              @endforeach
            </tbody>
    </table>
<div id="create-list" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Element Information</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="/empmgmt/admin_list/{{$id}}"> 
                        {!! csrf_field() !!}
                                    <div class="row">
                                        <label>Item Name</label>
                                        <input class="form-control" name="list_name" type="text" placeholder="Please type the Job Title here">
                                        <input type="hidden" value="{{$id}}" name="element_id">
                                    </div><br />
                            <button class="btn btn-md btn-primary" type="submit"> Save</button>
                        </form>  
                    </div>
            </div>
</div>
</div>
</div>

@endsection
@section('pagejs')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});
</script>
@endsection