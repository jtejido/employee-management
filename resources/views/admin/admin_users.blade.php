@extends('layouts.app')
@section('title', 'Admin Users')
@section('content')
@section('pagejs')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$( function() {
    $("#new_admin").autocomplete({
      scroll: true,
      autoFocus: true,
      minLength: 3,
      appendTo: "#add-user",
      source: function( request, response ) {
        $.ajax({
          url: "/empmgmt/admin_users/search",
          dataType: "json",
          data: {
              searchText: request.term
          },
          success: function( data ) {
           response($.map(data, function(item) {
                return {
                    label: item.name,
                    value: item.id
                };
            }));
          }
        });
      },
      select: function( event, ui) {
        $(this).val(ui.item.label);
        $("#new_admin_id").val(ui.item.value);
        return false;
      }
} );
} );
</script>
@endsection
    <h1 class="page-header">Admin Users</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to add, or remove users as admin of this site.</p>
    <div class="row">
    <div class="col-md-5">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-user">Add User</button>
    </div>
    </div>
    <br><br>
        <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>WIN ID</th>
                <th>Name</th>
                <th>Manager</th>
                <th>Location</th>
                <th>Client</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($admins as $key => $value)
              <tr>
                <td>{{ $value->username }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->manager }}</td>
                <td>{{ $value->site }}</td>
                <td>{{ $value->client }}</td>
                <td>
                <a href="/empmgmt/employee/{{$value->empl_id}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>
                <a href="/empmgmt/admin_users/{{ $value->empl_id }}/remove" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-trash"></i> Remove</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
<div id="add-user" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Admin User</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="/empmgmt/admin_users"> 
                        {!! csrf_field() !!}
                                <div class="form-group">
                                <div class="ui-widget">
                                  <label for="new_admin">New Manager</label>
                                  <input class="form-control" id="new_admin" name="new_admin" placeholder="Type a Name">
                                  <input type="hidden" id="new_admin_id" name="new_admin_id">
                                </div>
                                </div>
                            <button class="btn btn-md btn-primary" type="submit"> Save</button>
                        </form>  
                    </div>
            </div>
</div>
</div>
</div>

@endsection
@push('js')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});
</script>
@endpush