@extends('layouts.app')
@section('title', 'Admin Source Systems')
@section('content')
    <h1 class="page-header">Admin Source Systems</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to Manage Source Systems for this site. You can <b><i>ignore</i></b> and <b><i>unignore</i></b> a Source System to remove it from the <code>Unmapped IDs</code> page totally and Map existing Source Systems to their respective Clients. Changes made here will affect how the <code>Employee's profile</code> page and <code>Unmapped IDs</code> page is displayed. </p>
    <div class="row">
    <div class="col-md-5">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-client-integ">Map Source System to Client</button>
    </div>
    </div>
    <br><br>
        <table class="table table-bordered" id="sortable">
            <thead>
              <tr>
                <th>Client</th>
                <th>Source System</th>
                <th>Ignored?</th>
                <th>Date Created</th>
                <th>Date Updated</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($integrations as $key => $value)
              <tr>
                <td>{{ $value->client }}</td>
                <td>{{ $value->source }}</td>
                <td>
                  @if($value->ignore == 1)
                  Yes
                  @else
                  No
                  @endif
                </td>
                <td>{{ $value->created_at }}</td>
                <td>{{ $value->updated_at }}</td>
                <td>
                <a href="/empmgmt/admin_unmapped/{{ $value->client_id }}/{{ $value->src_type_id }}/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
<div id="add-client-integ" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Map Source System - Client</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="/empmgmt/admin_unmapped"> 
                        {!! csrf_field() !!}
                              <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                  <label for="new_client_id">Select Client/s</label>
                                  <select class="form-control" id="clients" name="new_client_id[]" multiple>
                                    @foreach($clients_dd as $key => $value)
                                    <option value="{!! $value->client_id !!}">{!! $value->client !!}</option>
                                    @endforeach
                                  </select>
                                  <br />

                                  <label for="new_src_type_id">Select Source System/s</label>
                                  <select class="form-control" id="sources" name="new_src_type_id[]" multiple>
                                    @foreach($sources_dd as $key => $value)
                                    <option value="{!! $value->src_type_id !!}">{!! $value->source !!}</option>
                                    @endforeach
                                  </select>
                                  <br />

                                </div>
                                <div class="col-md-12">
                                <label for="new_admin">Ignore when Mapping ID?</label>
                                <select class="form-control" name="ignore">
                                <option value="1">True</option>
                                <option value="0">False</option>
                                </select>
                                </div>
                              </div>
                                <br /><br />
                            <button class="btn btn-md btn-primary" type="submit"> Save</button>

                        </form>  
                    </div>
            </div>
</div>
</div>
</div>

@endsection
@push('js')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available"
        }
} );
});
</script>
@endpush