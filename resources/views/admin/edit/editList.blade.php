@extends('layouts.app')
@section('title', 'List Administration')
@section('content')
    <h1 class="page-header">List Administration</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to add, edit or remove items from the list shown to users who are attempting to add it to another user's Profile page.</p>
    <br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <form class="form-inline" method="POST">
                <p>You can make changes to the item from here. All changes are recorded.</p>
                <table class="table table-bordered table-hover table-scrolly">
                  <thead>
                    <th>List Name</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input type="text" class="input-sm form-control" name="list_name" value="{{ $list->list_name }}"/></td>
                      <td>{{ $list->created_at }}</td>
                      <td>{{ $list->updated_at }}</td>
                    </tr>
                  </tbody>
                </table>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Update</button>
              </form>
            </div>
          </div>
        </div>  
@endsection