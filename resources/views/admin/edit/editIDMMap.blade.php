@extends('layouts.app')
@section('title', 'Admin Source Systems')
@section('content')
    <h1 class="page-header">Elements Administration</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
     <p>This page is used to Manage Source Systems for this site. You can <b><i>ignore</i></b> and <b><i>unignore</i></b> a Source System to remove it from the <code>Unmapped IDs</code> page totally and Map existing Source Systems to their respective Clients. Changes made here will affect how the <code>Employee's profile</code> page and <code>Unmapped IDs</code> page is displayed. </p>
    <br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <form class="form-inline" method="POST">
                <p>You can make changes to the item from here. All changes are recorded.</p>

                <table class="table table-bordered table-hover table-scrolly">
                  <thead>
                  <th>Client</th>
                  <th>Source System</th>
                  <th>Ignored?</th>
                  <th>Date Created</th>
                  <th>Date Updated</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{{ $integ_client_map->client }}</td>
                      <td>{{ $integ_client_map->source }}</td>
                      <td>
                      <select class="input-sm form-control" name="ignore">
                      <option value=1 @if($integ_client_map->ignore == 1) selected @endif>Yes</option>
                      <option value=0 @if($integ_client_map->ignore == 0) selected @endif>No</option>
                      </select>
                      </td>
                      <td>{{ $integ_client_map->created_at }}</td>
                      <td>{{ $integ_client_map->updated_at }}</td>
                    </tr>
                  </tbody>
                </table>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Update</button>
              </form>
            </div>
          </div>
        </div>  
@endsection