@extends('layouts.app')
@section('title', 'Elements Administration')
@section('content')
    <h1 class="page-header">Elements Administration</h1>
    @if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {!! session('message') !!}
    </div>
    @endif
    <p>This page is used to add, edit or remove Element items shown to users who are attempting to add Elements to another user's Profile page.</p>
    <br><br>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <form class="form-inline" method="POST">
                <p>You can make changes to the item from here. All changes are recorded.</p>

                <table class="table table-bordered table-hover table-scrolly">
                  <thead>
                    <th>Element Name</th>
                    <th>Is Element a List?</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input type="text" class="input-sm form-control" name="element_name" value="{{ $element->element_name }}"/></td>
                      <td>
                      <select class="input-sm form-control" name="is_list">
                      <option value=1 @if($element->is_list == 1) selected @endif>Yes</option>
                      <option value=0 @if($element->is_list == 0) selected @endif>No</option>
                      </select>
                      </td>
                      <td>{{ $element->created_at }}</td>
                      <td>{{ $element->updated_at }}</td>
                    </tr>
                  </tbody>
                </table>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-success">Update</button>
              </form>
            </div>
          </div>
        </div>  
@endsection