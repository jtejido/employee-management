@extends('layouts.app')

@section('title', 'Employees')

@section('content')

@if((Session::has('search')) && (session('search.newhire') != true))
  <h2 class="page-header">Search Results</h2>
  <p>You are veiwing your search results. <a href="/empmgmt/clearsearch" class="btn btn-success">Clear Search</a></p>
@elseif((Session::has('search')) && (session('search.newhire') == true))
  <h2 class="page-header">Search Results</h2>
  <p>You are viewing all newhires within 30 days. <a href="/empmgmt/clearsearch" class="btn btn-success">Clear Search</a></p>
@else
<h2 class="page-header">View All Employees</h2>
<p>The employees listed here are the employees you have access to.</p>
@endif


  {!! $html->table() !!}
@endsection

@section('pagejs')
  <script type="text/javascript">
    $.extend(true, $.fn.dataTable.defaults, {
        language: {
          "search": "Filter records:"
        }
    });
</script>

  {!! $html->scripts() !!}
@endsection
