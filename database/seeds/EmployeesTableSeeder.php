<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i = 1; $i <= 1000; $i++) {
        DB::table('employees')->insert([
          'empl_id' => rand(),
          'name' => str_random(10),
          'client' => str_random(10),
          'site' => str_random(10),
          'manager' => str_random(10),
          'manager_id' => rand(1,10000),
          'site_id' => rand(1,111),
          'subcontract_id' => rand(1,111),
          'client_id' => rand(1,111),
          'hire_dte' => '2016-01-01',
          'term_dte' => '2016-01-01'
        ]);
      }
    }
}
