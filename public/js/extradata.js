$("#element_name").on('change', function(){
  if($(this).val() == 2){
        $("#element_value").datepicker();
    }
  else if($(this).val() == 1){
        $("#element_value").datepicker('destroy');
            $( function() {
                  $("#element_value").autocomplete({
                    scroll: true,
                    autoFocus: true,
                    minLength: 3,
                    source: function( request, response ) {
                      $.ajax({
                        url: "/employees/public/admin_list/search",
                        dataType: "json",
                        data: {
                            searchText: request.term
                        },
                        success: function( data ) {
                         response($.map(data, function(item) {
                              return {
                                  label: item.name,
                                  value: item.id
                              };
                          }));
                        }
                      });
                    },
                    select: function( event, ui) {
                      $(this).val(ui.item.label);
                      return false;
                    }
              } );
              } );
    }
  else {
      $('#element_value').datepicker('destroy');
    }
});
